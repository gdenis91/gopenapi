package utils

import (
	"github.com/fsnotify/fsnotify"
	"github.com/pkg/errors"
)

// StringPtr returns a pointer to the input string
func StringPtr(s string) *string {
	return &s
}

// BoolPtr returns a pointer to the input bool
func BoolPtr(b bool) *bool {
	return &b
}

// IntPtr returns a pointer to the input int
func IntPtr(i int) *int {
	return &i
}

// StringWithDefaultValue returns the string value if it is not nil, otherwise the default value is returned
func StringWithDefaultValue(value *string, defaultValue string) string {
	if value == nil {
		return defaultValue
	}
	return *value
}

// BoolWithDefaultValue returns the bool value if it is not nil, otherwise the default value is returned
func BoolWithDefaultValue(value *bool, defaultValue bool) bool {
	if value == nil {
		return defaultValue
	}
	return *value
}

// PrintClear prints characters to clear standard out
func PrintClear() {
	print("\033[H\033[2J")
}

func RegisterFileSaveHandler(filepath string, saveHandler func(), errHandler func(err error)) error {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		return errors.Wrap(err, "could not create new file watcher")
	}

	err = watcher.Add(filepath)
	if err != nil {
		return errors.Wrapf(err, "could not add filepath '%' to file watcher")
	}

	go func(watcher *fsnotify.Watcher) {
		defer watcher.Close()
		for {
			select {
			case event := <-watcher.Events:
				if event.Op == fsnotify.Rename || event.Op == fsnotify.Write {
					saveHandler()
					err = watcher.Add(filepath)
					if err != nil {
						errHandler(err)
					}
				}
			case err := <-watcher.Errors:
				errHandler(err)
			}
		}
	}(watcher)

	return nil
}
