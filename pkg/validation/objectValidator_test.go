package validation

import (
	. "gitlab.com/gdenis91/gopenapi/pkg/validation"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"errors"
)

var _ = Describe("ObjectValidator", func() {
	var (
		validator *ObjectValidator
	)
	BeforeEach(func() {
		validator = NewObjectValidator("testValidator")
	})

	Context("no errors appended", func() {
		BeforeEach(func() {
			validator.AppendMessageIfError(nil,"testError")
			validator.AppendMessageIf(false, "testError2")
		})
		It("has no errors", func() {
			Expect(validator.HasMessages()).To(BeFalse())
			Expect(validator.String()).To(ContainSubstring("No validation errors"))
		})
	})

	Context("errors appended", func() {
		BeforeEach(func() {
			validator.AppendMessageIfError(errors.New("err"),"testError0")
			validator.AppendMessage("testError1")
			validator.AppendMessageIf(true, "testError2")
		})
		It("has errors", func() {
			Expect(validator.HasMessages()).To(BeTrue())
			Expect(validator.String()).To(ContainSubstring("testError0"))
			Expect(validator.String()).To(ContainSubstring("testError1"))
			Expect(validator.String()).To(ContainSubstring("testError2"))
		})
	})

	Context("nested validation appended", func() {
		var (
			nestedValidation *ObjectValidator
		)
		BeforeEach(func() {
			nestedValidation = validator.AppendNestedValidationWithPrefix("nestedValidator")
		})
		Context("nested validator has no errors", func() {
			It("has no errors", func() {
				Expect(validator.HasMessages()).To(BeFalse())
			})
		})

		Context("nested validator has errors", func() {
			BeforeEach(func() {
				nestedValidation.AppendMessage("testNestedError")
			})
			It("has errors", func() {
				Expect(validator.HasMessages()).To(BeTrue())
				Expect(validator.String()).To(ContainSubstring("testNestedError"))
			})
		})
	})

	Describe("RequireAttribute", func() {
		Context("required attribute is nil", func () {
			BeforeEach(func() {
				validator.RequireAttribute("testAttr", nil)
			})
			It("fails validation", func() {
				Expect(validator.HasMessages()).To(BeTrue())
				Expect(validator.String()).To(ContainSubstring("'testAttr' is a required attribute"))
			})
		})

		Context("required attribute is not nil", func () {
			BeforeEach(func() {
				validator.RequireAttribute("testAttrString", "foo")
				validator.RequireAttribute("testAttrNumber", 100)
				validator.RequireAttribute("testAttrBool", true)
				validator.RequireAttribute("testAttrPtr", stringPtr("bar"))
			})
			It("passes validation", func() {
				Expect(validator.HasMessages()).To(BeFalse())
			})
		})
	})

	Describe("RequireExactString", func() {
		Context("required value is nil", func () {
			BeforeEach(func() {
				validator.RequireExactString("testAttr", "foo", nil)
				var emptyString *string
				validator.RequireExactString("testAttr1", "foo", emptyString)
			})
			It("fails validation", func() {
				Expect(validator.HasMessages()).To(BeTrue())
				Expect(validator.String()).To(ContainSubstring("'testAttr' is required to equal exactly 'foo', but was nil"))
				Expect(validator.String()).To(ContainSubstring("'testAttr1' is required to equal exactly 'foo', but was nil"))
			})
		})

		Context("required value is not a string or string pointer", func () {
			BeforeEach(func() {
				validator.RequireExactString("testAttrBool", "foo", true)
				validator.RequireExactString("testAttrNumber", "foo", 10)
				validator.RequireExactString("testAttrNumberPointer", "foo", intPtr(10))
			})
			It("fails validation", func() {
				Expect(validator.HasMessages()).To(BeTrue())
				Expect(validator.String()).To(ContainSubstring("'testAttrBool' is required to equal exactly 'foo', but was not a string"))
				Expect(validator.String()).To(ContainSubstring("'testAttrNumber' is required to equal exactly 'foo', but was not a string"))
				Expect(validator.String()).To(ContainSubstring("'testAttrNumberPointer' is required to equal exactly 'foo', but was not a string"))
			})
		})

		Context("required value is a string", func () {
			Context("has correct value", func() {
				BeforeEach(func() {
					validator.RequireExactString("testAttr", "foo", "foo")
				})
				It("passes validation", func() {
					Expect(validator.HasMessages()).To(BeFalse())
				})
			})
			Context("has incorrect value", func() {
				BeforeEach(func() {
					validator.RequireExactString("testAttr", "foo", "bar")
				})
				It("fails validation", func() {
					Expect(validator.HasMessages()).To(BeTrue())
					Expect(validator.String()).To(ContainSubstring("'testAttr' is required to equal exactly 'foo', but was 'bar'"))
				})
			})
		})

		Context("required value is a string pointer", func () {
			Context("has correct value", func() {
				BeforeEach(func() {
					validator.RequireExactString("testAttr", "foo", stringPtr("foo"))
				})
				It("passes validation", func() {
					Expect(validator.HasMessages()).To(BeFalse())
				})
			})
			Context("has incorrect value", func() {
				BeforeEach(func() {
					validator.RequireExactString("testAttr", "foo", stringPtr("bar"))
				})
				It("fails validation", func() {
					Expect(validator.HasMessages()).To(BeTrue())
					Expect(validator.String()).To(ContainSubstring("'testAttr' is required to equal exactly 'foo', but was 'bar'"))
				})
			})
		})
	})

	Describe("RequireNonEmptyString", func() {
		Context("required value is nil", func () {
			BeforeEach(func() {
				validator.RequireNonEmptyString("testAttr", nil)
				var emptyString *string
				validator.RequireNonEmptyString("testAttr1", emptyString)
			})
			It("fails validation", func() {
				Expect(validator.HasMessages()).To(BeTrue())
				Expect(validator.String()).To(ContainSubstring("'testAttr' is required, but was nil"))
				Expect(validator.String()).To(ContainSubstring("'testAttr1' is required, but was nil"))
			})
		})

		Context("required value is not a string or string pointer", func () {
			BeforeEach(func() {
				validator.RequireNonEmptyString("testAttrBool", true)
				validator.RequireNonEmptyString("testAttrNumber", 10)
				validator.RequireNonEmptyString("testAttrNumberPointer", intPtr(10))
			})
			It("fails validation", func() {
				Expect(validator.HasMessages()).To(BeTrue())
				Expect(validator.String()).To(ContainSubstring("'testAttrBool' must be a string"))
				Expect(validator.String()).To(ContainSubstring("'testAttrNumber' must be a string"))
				Expect(validator.String()).To(ContainSubstring("'testAttrNumberPointer' must be a string"))
			})
		})

		Context("required value is a string", func () {
			Context("has correct value", func() {
				BeforeEach(func() {
					validator.RequireNonEmptyString("testAttr", "foo")
				})
				It("passes validation", func() {
					Expect(validator.HasMessages()).To(BeFalse())
				})
			})
			Context("has incorrect value", func() {
				BeforeEach(func() {
					validator.RequireNonEmptyString("testAttr0", " ")
				})
				It("fails validation", func() {
					Expect(validator.HasMessages()).To(BeTrue())
					Expect(validator.String()).To(ContainSubstring("'testAttr0' must be non-empty"))
				})
			})
		})

		Context("required value is a string pointer", func () {
			Context("has correct value", func() {
				BeforeEach(func() {
					validator.RequireNonEmptyString("testAttr", stringPtr("foo"))
				})
				It("passes validation", func() {
					Expect(validator.HasMessages()).To(BeFalse())
				})
			})
			Context("has incorrect value", func() {
				BeforeEach(func() {
					validator.RequireNonEmptyString("testAttr", stringPtr(" "))
				})
				It("fails validation", func() {
					Expect(validator.HasMessages()).To(BeTrue())
					Expect(validator.String()).To(ContainSubstring("'testAttr' must be non-empty"))
				})
			})
		})
	})
})
