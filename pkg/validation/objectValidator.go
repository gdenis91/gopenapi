package validation

import (
	"fmt"
	"strings"
	"reflect"
)

// NewObjectValidator creates a new validator with the specified prefix
func NewObjectValidator(prefix string) *ObjectValidator {
	return &ObjectValidator{
		prefix: prefix,
	}
}

// ObjectValidator validates the spec
type ObjectValidator struct {
	prefix             string
	validationMessages []string
	nestedValidations  []*ObjectValidator
}

// AppendMessage appends a message to the validator
func (v *ObjectValidator) AppendMessage(message string) {
	v.validationMessages = append(v.validationMessages, message)
}

// AppendMessageIfError appends an error message if err is not nil
func (v *ObjectValidator) AppendMessageIfError(err error, message string) {
	if err != nil {
		v.validationMessages = append(v.validationMessages, message)
	}
}

// AppendMessageIf appends an error message if specified condition is true
func (v *ObjectValidator) AppendMessageIf(shouldAppend bool, message string) {
	if shouldAppend {
		v.validationMessages = append(v.validationMessages, message)
	}
}

// HasMessages returns true if the validator or any nested validators have an error
func (v *ObjectValidator) HasMessages() bool {
	for _, nestedValidation := range v.nestedValidations {
		if nestedValidation.HasMessages() {
			return true
		}
	}
	return len(v.validationMessages) > 0
}

// AppendNestedValidationWithPrefix appends a nested validator with an added prefix
func (v *ObjectValidator) AppendNestedValidationWithPrefix(prefix string) *ObjectValidator {
	nestedValidator := NewObjectValidator(v.prefix + ":" + prefix)
	v.nestedValidations = append(v.nestedValidations, nestedValidator)
	return nestedValidator
}

// RequireAttribute appends an error if the passed attribute is nil
func (v *ObjectValidator) RequireAttribute(attributeName string, i interface{}) {
	if isNil(i) {
		v.AppendMessage(fmt.Sprintf("'%s' is a required attribute, but was nil", attributeName))
	}
}

// RequireExactString appends an error if the passed attribute does not match the required string
func (v *ObjectValidator) RequireExactString(attributeName string, value string, i interface{}) {
	if isNil(i) {
		v.AppendMessage(fmt.Sprintf("'%s' is required to equal exactly '%s', but was nil", attributeName, value))
		return
	}
	switch iVal := i.(type) {
	case string:
		if iVal != value {
			v.AppendMessage(fmt.Sprintf("'%s' is required to equal exactly '%s', but was '%s'", attributeName, value, iVal))
		}
	case *string:
		if *iVal != value {
			v.AppendMessage(fmt.Sprintf("'%s' is required to equal exactly '%s', but was '%s'", attributeName, value, *iVal))
		}
	default:
		v.AppendMessage(fmt.Sprintf("'%s' is required to equal exactly '%s', but was not a string", attributeName, value))
	}
}

// RequireNonEmptyString appends an error if the passed attribute is an empty string
func (v *ObjectValidator) RequireNonEmptyString(attributeName string, i interface{}) {
	if isNil(i) {
		v.AppendMessage(fmt.Sprintf("'%s' is required, but was nil", attributeName))
		return
	}
	switch iVal := i.(type) {
	case string:
		if len(strings.TrimSpace(iVal)) == 0 {
			v.AppendMessage(fmt.Sprintf("'%s' must be non-empty", attributeName))
		}
	case *string:
		if len(strings.TrimSpace(*iVal)) == 0 {
			v.AppendMessage(fmt.Sprintf("'%s' must be non-empty", attributeName))
		}
	default:
		v.AppendMessage(fmt.Sprintf("'%s' must be a string", attributeName))
	}
}

func (v *ObjectValidator) String() string {
	if !v.HasMessages() {
		return "No validation errors"
	}
	messages := v.GetMessages()

	return strings.Join(messages, "\n")
}

// GetMessages returns all appended messages
func (v *ObjectValidator) GetMessages () []string {
	var messages []string

	for _, m := range v.validationMessages {
		messages = append(messages, v.prefix+": "+m)
	}

	for _, v := range v.nestedValidations {
		if v.HasMessages() {
			messages = append(messages, v.String())
		}
	}
	return messages
}

func isNil(a interface{}) bool {
	defer func() { recover() }()
	return a == nil || reflect.ValueOf(a).IsNil()
}