package spec

import (
	"fmt"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
)

// Components https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md#components-object
type Components struct {
	Schemas         map[string]Schema         `yaml:"schemas"`
	Responses       *Responses                 `yaml:"responses"`
	Parameters      map[string]Parameter      `yaml:"parameters"`
	Examples        map[string]Example        `yaml:"examples"`
	RequestBodies   map[string]RequestBody    `yaml:"requestBodies"`
	Headers         map[string]Header         `yaml:"headers"`
	SecuritySchemes map[string]SecurityScheme `yaml:"securitySchemes"`
	Links           map[string]Link           `yaml:"links"`
	Callbacks       map[string]Callback       `yaml:"callbacks"`
}

func (c *Components) Validate(validator *validation.ObjectValidator) *validation.ObjectValidator {
	for k := range c.Schemas {
		validator.AppendMessageIf(!componentKeyRegExp.MatchString(k), fmt.Sprintf("'schema' attribute key '%s' must match regular expression '%s'", componentKeyRegExp.String()))
	}
	if c.Responses != nil {
		for k := range *c.Responses {
			validator.AppendMessageIf(!componentKeyRegExp.MatchString(k), fmt.Sprintf("'responses' attribute key '%s' must match regular expression '%s'", componentKeyRegExp.String()))
		}
	}
	for k := range c.Parameters {
		validator.AppendMessageIf(!componentKeyRegExp.MatchString(k), fmt.Sprintf("'parameters' attribute key '%s' must match regular expression '%s'", componentKeyRegExp.String()))
	}
	for k := range c.Examples {
		validator.AppendMessageIf(!componentKeyRegExp.MatchString(k), fmt.Sprintf("'examples' attribute key '%s' must match regular expression '%s'", componentKeyRegExp.String()))
	}
	for k := range c.RequestBodies {
		validator.AppendMessageIf(!componentKeyRegExp.MatchString(k), fmt.Sprintf("'requestBodies' attribute key '%s' must match regular expression '%s'", componentKeyRegExp.String()))
	}
	for k := range c.Headers {
		validator.AppendMessageIf(!componentKeyRegExp.MatchString(k), fmt.Sprintf("'headers' attribute key '%s' must match regular expression '%s'", componentKeyRegExp.String()))
	}
	for k := range c.SecuritySchemes {
		validator.AppendMessageIf(!componentKeyRegExp.MatchString(k), fmt.Sprintf("'securitySchemes' attribute key '%s' must match regular expression '%s'", componentKeyRegExp.String()))
	}
	for k := range c.Links {
		validator.AppendMessageIf(!componentKeyRegExp.MatchString(k), fmt.Sprintf("'links' attribute key '%s' must match regular expression '%s'", componentKeyRegExp.String()))
	}
	for k := range c.Callbacks {
		validator.AppendMessageIf(!componentKeyRegExp.MatchString(k), fmt.Sprintf("'callbacks' attribute key '%s' must match regular expression '%s'", componentKeyRegExp.String()))
	}
	return validator
}
