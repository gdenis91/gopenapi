package spec

import (
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
	"net/url"
)

// ExternalDocumentation https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md#external-documentation-object
type ExternalDocumentation struct {
	Description *string `yaml:"description"`
	URL         *string `yaml:"url"`
}

// Validate validates the element
func (ed *ExternalDocumentation) Validate(validator *validation.ObjectValidator) *validation.ObjectValidator {
	validator.RequireNonEmptyString("name", ed.URL)

	if ed.URL != nil {
		_, err := url.Parse(*ed.URL)
		validator.AppendMessageIfError(err, "'url' attribute must be in the format of a url")
	}

	return validator
}
