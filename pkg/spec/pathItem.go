package spec

import (
	"fmt"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
	"gitlab.com/gdenis91/gopenapi/pkg/utils"
)

// PathItem https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md#path-item-object
type PathItem struct {
	Ref         *string     `yaml:"$ref"`
	Summary     *string     `yaml:"summary"`
	Description *string     `yaml:"description"`
	Get         *Operation  `yaml:"get"`
	Put         *Operation  `yaml:"put"`
	Post        *Operation  `yaml:"post"`
	Delete      *Operation  `yaml:"delete"`
	Options     *Operation  `yaml:"options"`
	Head        *Operation  `yaml:"head"`
	Patch       *Operation  `yaml:"patch"`
	Trace       *Operation  `yaml:"trace"`
	Servers     []Server    `yaml:"servers"`
	Parameters  []Parameter `yaml:"parameters"`
}

// Validate validates the element
func (pi *PathItem) Validate(validator *validation.ObjectValidator) *validation.ObjectValidator {
	usedParams := map[string]Parameter{}

	for _, param := range pi.Parameters {
		if _, found := usedParams[param.UniqueID()]; found {
			validator.AppendMessage(
				fmt.Sprintf("Duplicate param '%s' in '%s'. Each param must have a unique combination of 'name' and 'in' attribute values",
					utils.StringWithDefaultValue(param.Name, ""),
					utils.StringWithDefaultValue(param.In, "")))
		}

		usedParams[param.UniqueID()] = param
	}

	return validator
}
