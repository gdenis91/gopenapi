package spec

import (
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
)

// Responses https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md#responses-object
type Responses map[string]Response

// Validate validates the element
func (r *Responses) Validate(validator *validation.ObjectValidator) *validation.ObjectValidator {
	if len(*r) == 0 {
		validator.AppendMessage("Must contain at least one response code")
	}

	return validator
}
