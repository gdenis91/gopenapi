package spec

import (
	. "gitlab.com/gdenis91/gopenapi/domain/spec"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/gdenis91/gopenapi/pkg/utils"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
)

var _ = Describe("Server", func() {
	var (
		server    *Server
		validator *validation.ObjectValidator
	)

	BeforeEach(func() {
		validator = validation.NewObjectValidator("server")
	})

	Context("with required fields", func() {
		BeforeEach(func() {
			server = &Server{
				URL: utils.StringPtr("www.foo.com"),
			}
		})

		It("has no validation messages", func() {
			validator = server.Validate(validator)
			Expect(validator.HasMessages()).To(BeFalse())
		})

		Context("url attribute is nil", func() {
			BeforeEach(func() {
				server.URL = nil
			})
			It("has validation messages", func() {
				validator = server.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})
	})
})
