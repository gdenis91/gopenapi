package spec

import (
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
)

// MediaType https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md#media-type-object
type MediaType struct {
	Schema   *Schema             `yaml:"schema"`
	Example  *interface{}        `yaml:"example"`
	Examples map[string]Example  `yaml:"examples"`
	Encoding map[string]Encoding `yaml:"encoding"`
}

// Validate validates the element
func (mt *MediaType) Validate(validator *validation.ObjectValidator) *validation.ObjectValidator {
	return validator
}
