package spec

import (
	"fmt"
	"gitlab.com/gdenis91/gopenapi/pkg/utils"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
	"regexp"
	"gopkg.in/yaml.v2"
	"github.com/pkg/errors"
)

// Schema https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md#schema-object
type Schema struct {
	Ref                  *string           `yaml:"$ref"`
	Title                *string           `yaml:"title"`
	MultipleOf           *int              `yaml:"multipleOf"`
	Maximum              *int              `yaml:"maximum"`
	ExclusiveMaximum     *bool             `yaml:"exclusiveMaximum"`
	Minimum              *int              `yaml:"minimum"`
	ExclusiveMinimum     *bool             `yaml:"exclusiveMinimum"`
	MaxLength            *int              `yaml:"maxLength"`
	MinLength            *int              `yaml:"minLength"`
	Pattern              *string           `yaml:"pattern"`
	MaxItems             *int              `yaml:"maxItems"`
	MinItems             *int              `yaml:"minItems"`
	UniqueItems          *bool             `yaml:"uniqueItems"`
	MaxProperties        *int              `yaml:"maxProperties"`
	MinProperties        *int              `yaml:"minProperties"`
	Required             []string          `yaml:"required"`
	Enum                 []interface{}     `yaml:"enum"`
	Type                 *string           `yaml:"type"`
	AllOf                []Schema          `yaml:"allOf"`
	OneOf                []Schema          `yaml:"oneOf"`
	AnyOf                []Schema          `yaml:"anyOf"`
	Not                  *Schema           `yaml:"not"`
	Items                *Schema           `yaml:"items"`
	Properties           map[string]Schema `yaml:"properties"`
	AdditionalProperties *Schema           `yaml:"additionalProperties"`
	Description          *string           `yaml:"description"`
	Format               *string           `yaml:"format"`
	Default              interface{}       `yaml:"default"`
	Nullable             *bool             `yaml:"nullable"`
	Discriminator        *Discriminator    `yaml:"discriminator"`
	ReadOnly             *bool             `yaml:"readOnly"`
	WriteOnly            *bool             `yaml:"writeOnly"`
	//XML interface{} `yaml:"xml"` ToDo:Implement XML support
	ExternalDocs *ExternalDocumentation `yaml:"externalDocs"`
	Example      *interface{}           `yaml:"example"`
	Deprecated   *bool                  `yaml:"deprecated"`
}

// Validate validates the element
func (s *Schema) Validate(validator *validation.ObjectValidator) *validation.ObjectValidator {
	validator.AppendMessageIf(s.MultipleOf != nil && *s.MultipleOf <= 0, "'multipleOf' attribute should be a value greater than 0")
	validator.AppendMessageIf(s.MaxLength != nil && *s.MaxLength < 0, "'maxLength' attribute should be an integer greater than or equal to 0")
	validator.AppendMessageIf(s.MinLength != nil && *s.MinLength < 0, "'minLength' attribute should be an integer greater than or equal to 0")
	validator.AppendMessageIf(s.MaxItems != nil && *s.MaxItems < 0, "'maxItems' attribute should be an integer greater than or equal to 0")
	validator.AppendMessageIf(s.MinItems != nil && *s.MinItems < 0, "'minItems' attribute should be an integer greater than or equal to 0")
	validator.AppendMessageIf(s.MaxProperties != nil && *s.MaxProperties < 0, "'maxProperties' attribute should be an integer greater than or equal to 0")
	validator.AppendMessageIf(s.MinProperties != nil && *s.MinProperties < 0, "'minProperties' attribute should be an integer greater than or equal to 0")

	requiredProperties := map[string]string{}
	for _, val := range s.Required {
		if _, found := requiredProperties[val]; found {
			validator.AppendMessage(fmt.Sprintf("'required' attribute contains duplicate value '%s', entries must be unique", val))
		}
		requiredProperties[val] = val
	}

	if s.Pattern != nil {
		_, err := regexp.Compile(*s.Pattern)
		validator.AppendMessageIfError(err, "'pattern' attribute must be a valid regular expression")
	}

	if s.ReadOnly != nil && s.WriteOnly != nil {
		validator.AppendMessageIf(*s.ReadOnly && *s.WriteOnly, "'readOnly' and 'writeOnly' cannot both be true")
	}

	schemaType := SchemaType(utils.StringWithDefaultValue(s.Type, ""))
	if _, found := AllSchemaTypes[schemaType]; !found {
		validator.AppendMessage(fmt.Sprintf("'type' attribute was %s, must have one of the following values: 'string', 'number', 'integer', 'boolean', 'array', or 'object'", schemaType))
	}

	if schemaType == SchemaTypeArray {
		validator.AppendMessageIf(s.Items == nil, "If 'type' attribute is 'array' then the 'items' attribute must be specified")
	}

	return validator
}

func SchemaFromMap(schemaMap map[interface{}]interface{}) (*Schema, error) {
	docBytes, err := yaml.Marshal(schemaMap)
	if err != nil {
		return nil, errors.Wrap(err, "could not marshal doc map to yaml bytes")
	}
	schema := &Schema{}
	err = yaml.Unmarshal(docBytes, &schema)
	if err != nil {
		return nil, errors.Wrap(err, "could not unmarshal yaml bytes to schema")
	}
	return schema, nil
}