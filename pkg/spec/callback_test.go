package spec

import (
	. "gitlab.com/gdenis91/gopenapi/domain/spec"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
)

var _ = Describe("Callback", func() {
	var (
		callback  *Callback
		validator *validation.ObjectValidator
	)

	BeforeEach(func() {
		validator = validation.NewObjectValidator("callback")
	})

	Context("with required fields", func() {
		BeforeEach(func() {
			callback = &Callback{}
		})

		It("has no validation messages", func() {
			validator = callback.Validate(validator)
			Expect(validator.HasMessages()).To(BeFalse())
		})
	})
})
