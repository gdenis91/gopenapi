package spec

import (
	. "gitlab.com/gdenis91/gopenapi/domain/spec"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/gdenis91/gopenapi/pkg/utils"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
)

var _ = Describe("OpenAPIDoc", func() {
	var (
		openAPIDoc *OpenAPIDoc
		validator  *validation.ObjectValidator
	)

	BeforeEach(func() {
		validator = validation.NewObjectValidator("openapidoc")
	})

	Context("with required fields", func() {
		BeforeEach(func() {
			openAPIDoc = &OpenAPIDoc{
				OpenAPI: utils.StringPtr("3.0.0"),
				Info:    &Info{},
				Paths:   &Paths{},
			}
		})

		It("has no validation messages", func() {
			validator = openAPIDoc.Validate(validator)
			Expect(validator.HasMessages()).To(BeFalse())
		})

		Context("with tags values", func() {
			BeforeEach(func() {
				openAPIDoc.Tags = append(openAPIDoc.Tags, Tag{Name: utils.StringPtr("foo")})
				openAPIDoc.Tags = append(openAPIDoc.Tags, Tag{Name: utils.StringPtr("bar")})
				openAPIDoc.Tags = append(openAPIDoc.Tags, Tag{Name: utils.StringPtr("baz")})
			})
			It("has no validation messages", func() {
				validator = openAPIDoc.Validate(validator)
				Expect(validator.HasMessages()).To(BeFalse())
			})

			Context("with non-unique names", func() {
				BeforeEach(func() {
					openAPIDoc.Tags = append(openAPIDoc.Tags, Tag{Name: utils.StringPtr("foo")})
				})
				It("has validation messages", func() {
					validator = openAPIDoc.Validate(validator)
					Expect(validator.HasMessages()).To(BeTrue())
				})
			})
		})

		Context("openapi attribute not 3.0.0", func() {
			BeforeEach(func() {
				openAPIDoc.OpenAPI = utils.StringPtr("")
			})
			It("has validation messages", func() {
				validator = openAPIDoc.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})

		Context("info attribute is nil", func() {
			BeforeEach(func() {
				openAPIDoc.Info = nil
			})
			It("has validation messages", func() {
				validator = openAPIDoc.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})

		Context("paths attribute is nil", func() {
			BeforeEach(func() {
				openAPIDoc.Paths = nil
			})
			It("has validation messages", func() {
				validator = openAPIDoc.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})
	})
})
