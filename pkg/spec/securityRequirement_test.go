package spec

import (
	. "gitlab.com/gdenis91/gopenapi/domain/spec"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
)

var _ = Describe("SecurityRequirement", func() {
	var (
		securityRequirement *SecurityRequirement
		validator           *validation.ObjectValidator
	)

	BeforeEach(func() {
		validator = validation.NewObjectValidator("securityRequirement")
	})

	Context("with required fields", func() {
		BeforeEach(func() {
			securityRequirement = &SecurityRequirement{}
		})

		It("has no validation messages", func() {
			validator = securityRequirement.Validate(validator)
			Expect(validator.HasMessages()).To(BeFalse())
		})
	})
})
