package spec

import "gitlab.com/gdenis91/gopenapi/pkg/validation"

// ServerVariable https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md#server-variable-object
type ServerVariable struct {
	Enum        []string `yaml:"enum"`
	Default     *string  `yaml:"default"`
	Description *string  `yaml:"description"`
}

// Validate validates the element
func (sv *ServerVariable) Validate(validator *validation.ObjectValidator) *validation.ObjectValidator {
	validator.RequireNonEmptyString("default", sv.Default)

	return validator
}
