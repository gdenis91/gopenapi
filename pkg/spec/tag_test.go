package spec

import (
	. "gitlab.com/gdenis91/gopenapi/domain/spec"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/gdenis91/gopenapi/pkg/utils"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
)

var _ = Describe("Tag", func() {
	var (
		tag       *Tag
		validator *validation.ObjectValidator
	)

	BeforeEach(func() {
		validator = validation.NewObjectValidator("tag")
	})

	Context("with required fields", func() {
		BeforeEach(func() {
			tag = &Tag{
				Name: utils.StringPtr("foo"),
			}
		})

		It("has no validation messages", func() {
			validator = tag.Validate(validator)
			Expect(validator.HasMessages()).To(BeFalse())
		})

		Context("name attribute is nil", func() {
			BeforeEach(func() {
				tag.Name = nil
			})
			It("has validation messages", func() {
				validator = tag.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})
	})
})
