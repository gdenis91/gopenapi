package spec

import (
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
	"net/mail"
	"net/url"
)

// Contact https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md#contact-object
type Contact struct {
	Name  *string `yaml:"name"`
	URL   *string `yaml:"url"`
	Email *string `yaml:"email"`
}

// Validate validates the element
func (c *Contact) Validate(validator *validation.ObjectValidator) *validation.ObjectValidator {
	if c.URL != nil {
		_, err := url.Parse(*c.URL)
		validator.AppendMessageIfError(err, "'url' attribute must be in the format of a url")
	}

	if c.Email != nil {
		_, err := mail.ParseAddress(*c.Email)
		validator.AppendMessageIfError(err, "'email' attribute must be in the format of an email address")
	}

	return validator
}
