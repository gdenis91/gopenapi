package spec

import (
	. "gitlab.com/gdenis91/gopenapi/domain/spec"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
)

var _ = Describe("RequestBody", func() {
	var (
		requestBody *RequestBody
		validator   *validation.ObjectValidator
	)

	BeforeEach(func() {
		validator = validation.NewObjectValidator("requestBody")
	})

	Context("with required fields", func() {
		BeforeEach(func() {
			requestBody = &RequestBody{
				Content: map[string]MediaType{
					"foo": {},
				},
			}
		})

		It("has no validation messages", func() {
			validator = requestBody.Validate(validator)
			Expect(validator.HasMessages()).To(BeFalse())
		})

		Context("content attribute is nil", func() {
			BeforeEach(func() {
				requestBody.Content = nil
			})
			It("has validation messages", func() {
				validator = requestBody.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})

		Context("content attribute with no entries", func() {
			BeforeEach(func() {
				requestBody.Content = map[string]MediaType{}
			})
			It("has validation messages", func() {
				validator = requestBody.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})
	})
})
