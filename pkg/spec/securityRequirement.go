package spec

import "gitlab.com/gdenis91/gopenapi/pkg/validation"

// SecurityRequirement https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md#security-requirement-object
type SecurityRequirement map[string][]string

// Validate validates the element
func (sr *SecurityRequirement) Validate(validator *validation.ObjectValidator) *validation.ObjectValidator {
	return validator
}
