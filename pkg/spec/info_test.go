package spec

import (
	. "gitlab.com/gdenis91/gopenapi/domain/spec"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/gdenis91/gopenapi/pkg/utils"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
)

var _ = Describe("Info", func() {
	var (
		info      *Info
		validator *validation.ObjectValidator
	)

	BeforeEach(func() {
		validator = validation.NewObjectValidator("info")
	})

	Context("with required fields", func() {
		BeforeEach(func() {
			info = &Info{
				Title:   utils.StringPtr("Test info"),
				Version: utils.StringPtr("v0.0"),
			}
		})

		It("has no validation messages", func() {
			validator = info.Validate(validator)
			Expect(validator.HasMessages()).To(BeFalse())
		})

		Context("with termsOfService attribute", func() {
			Context("which is a url", func() {
				BeforeEach(func() {
					info.TermsOfService = utils.StringPtr("http://www.foo.com/")
				})
				It("has no validation messages", func() {
					validator = info.Validate(validator)
					Expect(validator.HasMessages()).To(BeFalse())
				})
			})

			Context("which is not a url", func() {
				BeforeEach(func() {
					info.TermsOfService = utils.StringPtr("fiz;#wwfoo.com%*&()/")
				})
				It("has validation messages", func() {
					validator = info.Validate(validator)
					Expect(validator.HasMessages()).To(BeTrue())
				})
			})
		})

		Context("title attribute is nil", func() {
			BeforeEach(func() {
				info.Title = nil
			})
			It("has validation messages", func() {
				validator = info.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})

		Context("version attribute is nil", func() {
			BeforeEach(func() {
				info.Version = nil
			})
			It("has validation messages", func() {
				validator = info.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})
	})
})
