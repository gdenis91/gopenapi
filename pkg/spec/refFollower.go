package spec

import (
	"fmt"
	"github.com/pkg/errors"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"net/url"
	"strings"
)

type RefFollower struct {
	rootDoc      map[interface{}]interface{}
	specRootPath string
}

func (r *RefFollower) FollowRef(ref string, val interface{}) error {
	refURL, err := url.Parse(ref)
	if err != nil {
		return errors.Wrapf(err, "could not parse ref '%s'", ref)
	}

	workingDoc := r.rootDoc

	// If there is a path then the referenced object is located in a separate doc
	// so we should use that value instead of the root doc
	if len(refURL.Path) > 0 {
		externalDoc, err := r.getExternalDoc(refURL)
		if err != nil {
			return err
		}
		workingDoc = externalDoc
	}


	// 1) Get the referenced Object (Internal ref, relative external ref, absolute external ref)
	// 2) If there is no fragment then the referenced doc is the object
	// 3) If there is a fragment then the nested attribute referenced is the object

	fragmentParts := strings.Split(strings.Trim(refURL.Fragment, "/"), "/")
	for i, part := range fragmentParts {
		nestedObject, found := curMap[part]
		if !found {
			return errors.New(fmt.Sprintf("attribute '%s' not found", part))
		}

		objectMap, isMap := nestedObject.(map[interface{}]interface{})
		if !isMap {
			return errors.New(fmt.Sprintf("attribute '%s' was not an object", part))
		}

		// If this is the last part of the fragment it's the object we want
		if i == len(fragmentParts)-1 {
			switch val.(type) {
			case *Schema:
				schema, err := SchemaFromMap(objectMap)
				if err != nil {
					return err
				}
				val = schema
				return nil
			case *Response:
				response, err := ResponseFromMap(objectMap)
				if err != nil {
					return err
				}
				val = response
				return nil
			case *Parameter:
				parameter, err := ParameterFromMap(objectMap)
				if err != nil {
					return err
				}
				val = parameter
				return nil
			case *Example:
				example, err := ExampleFromMap(objectMap)
				if err != nil {
					return err
				}
				val = example
				return nil
			case *RequestBody:
				requestBody, err := RequestBodyFromMap(objectMap)
				if err != nil {
					return err
				}
				val = requestBody
				return nil
			case *Header:
				header, err := HeaderFromMap(objectMap)
				if err != nil {
					return err
				}
				val = header
				return nil
			case *SecurityScheme:
				securityScheme, err := SecuritySchemeFromMap(objectMap)
				if err != nil {
					return err
				}
				val = securityScheme
				return nil
			case *Link:
				link, err := LinkFromMap(objectMap)
				if err != nil {
					return err
				}
				val = link
				return nil
			case *Callback:
				link, err := CallbackFromMap(objectMap)
				if err != nil {
					return err
				}
				val = link
				return nil
			}
		}
		curMap = objectMap
	}

	return nil
}

func (r *RefFollower) getExternalDoc(refURL *url.URL) (map[interface{}]interface{}, error) {
	// ToDO: Need to account for root path. Should be wherever the root doc is located
	refBytes, err := ioutil.ReadFile(r.specRootPath + refURL.Path)
	if err != nil {
		return nil, errors.Wrapf(err, "could not read file at location '%s'", refURL.Path)
	}
	refMap := map[interface{}]interface{}{}
	err = yaml.Unmarshal(refBytes, refMap)
	if err != nil {
		return nil, errors.Wrapf(err, "could not unmarshal object at location '%s'", refURL.Path)
	}
	return refMap, nil
}

