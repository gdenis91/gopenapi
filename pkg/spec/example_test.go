package spec

import (
	. "gitlab.com/gdenis91/gopenapi/domain/spec"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
)

var _ = Describe("Example", func() {
	var (
		example   *Example
		validator *validation.ObjectValidator
	)

	BeforeEach(func() {
		validator = validation.NewObjectValidator("example")
	})

	Context("with required fields", func() {
		BeforeEach(func() {
			example = &Example{}
		})

		It("has no validation messages", func() {
			validator = example.Validate(validator)
			Expect(validator.HasMessages()).To(BeFalse())
		})
	})
})
