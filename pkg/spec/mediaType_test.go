package spec

import (
	. "gitlab.com/gdenis91/gopenapi/domain/spec"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
)

var _ = Describe("MediaType", func() {
	var (
		mediaType *MediaType
		validator *validation.ObjectValidator
	)

	BeforeEach(func() {
		validator = validation.NewObjectValidator("mediaType")
	})

	Context("with required fields", func() {
		BeforeEach(func() {
			mediaType = &MediaType{}
		})

		It("has no validation messages", func() {
			validator = mediaType.Validate(validator)
			Expect(validator.HasMessages()).To(BeFalse())
		})
	})
})
