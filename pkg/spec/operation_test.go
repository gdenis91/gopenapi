package spec

import (
	. "gitlab.com/gdenis91/gopenapi/domain/spec"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/gdenis91/gopenapi/pkg/utils"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
)

var _ = Describe("Operation", func() {
	var (
		operation *Operation
		validator *validation.ObjectValidator
	)

	BeforeEach(func() {
		validator = validation.NewObjectValidator("operation")
	})

	Context("with required fields", func() {
		BeforeEach(func() {
			operation = &Operation{
				Responses: &Responses{},
			}
		})

		It("has no validation messages", func() {
			validator = operation.Validate(validator)
			Expect(validator.HasMessages()).To(BeFalse())
		})

		Context("responses attribute is nil", func() {
			BeforeEach(func() {
				operation.Responses = nil
			})
			It("has validation messages", func() {
				validator = operation.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})

		Context("with parameter values", func() {
			BeforeEach(func() {
				operation.Parameters = append(operation.Parameters, Parameter{Name: utils.StringPtr("foo"), In: utils.StringPtr("foo")})
				operation.Parameters = append(operation.Parameters, Parameter{Name: utils.StringPtr("bar"), In: utils.StringPtr("bar")})
				operation.Parameters = append(operation.Parameters, Parameter{Name: utils.StringPtr("baz"), In: utils.StringPtr("baz")})

			})
			It("has no validation messages", func() {
				validator = operation.Validate(validator)
				Expect(validator.HasMessages()).To(BeFalse())
			})

			Context("with non-unique parameters", func() {
				BeforeEach(func() {
					operation.Parameters = append(operation.Parameters, Parameter{Name: utils.StringPtr("foo"), In: utils.StringPtr("foo")})
					operation.Parameters = append(operation.Parameters, Parameter{Name: utils.StringPtr("foo"), In: utils.StringPtr("baz")})
					operation.Parameters = append(operation.Parameters, Parameter{Name: utils.StringPtr("baz"), In: utils.StringPtr("baz")})
				})
				It("has validation messages", func() {
					validator = operation.Validate(validator)
					Expect(validator.HasMessages()).To(BeTrue())
					Expect(validator.GetMessages()).To(HaveLen(2))
				})
			})
		})

	})
})
