package spec

import (
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
	"net/url"
)

// License https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md#license-object
type License struct {
	Name *string `yaml:"name"`
	URL  *string `yaml:"url"`
}

// Validate validates the element
func (l *License) Validate(validator *validation.ObjectValidator) *validation.ObjectValidator {
	validator.RequireNonEmptyString("name", l.Name)

	if l.URL != nil {
		_, err := url.Parse(*l.URL)
		validator.AppendMessageIfError(err, "'url' attribute must be in the format of a url")
	}

	return validator
}
