package spec

import (
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
)

// Server https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md#server-object
type Server struct {
	URL         *string                   `yaml:"url"`
	Description *string                   `yaml:"description"`
	Variables   map[string]ServerVariable `yaml:"variables"`
}

// Validate validates the element
func (s *Server) Validate(validator *validation.ObjectValidator) *validation.ObjectValidator {
	validator.RequireNonEmptyString("url", s.URL)

	return validator
}
