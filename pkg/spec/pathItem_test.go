package spec

import (
	. "gitlab.com/gdenis91/gopenapi/domain/spec"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/gdenis91/gopenapi/pkg/utils"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
)

var _ = Describe("PathItem", func() {
	var (
		pathItem  *PathItem
		validator *validation.ObjectValidator
	)

	BeforeEach(func() {
		validator = validation.NewObjectValidator("pathItem")
	})

	Context("with required fields", func() {
		BeforeEach(func() {
			pathItem = &PathItem{}
		})

		It("has no validation messages", func() {
			validator = pathItem.Validate(validator)
			Expect(validator.HasMessages()).To(BeFalse())
		})

		Context("with parameter values", func() {
			BeforeEach(func() {
				pathItem.Parameters = append(pathItem.Parameters, Parameter{Name: utils.StringPtr("foo"), In: utils.StringPtr("foo")})
				pathItem.Parameters = append(pathItem.Parameters, Parameter{Name: utils.StringPtr("bar"), In: utils.StringPtr("bar")})
				pathItem.Parameters = append(pathItem.Parameters, Parameter{Name: utils.StringPtr("baz"), In: utils.StringPtr("baz")})

			})
			It("has no validation messages", func() {
				validator = pathItem.Validate(validator)
				Expect(validator.HasMessages()).To(BeFalse())
			})

			Context("with non-unique parameters", func() {
				BeforeEach(func() {
					pathItem.Parameters = append(pathItem.Parameters, Parameter{Name: utils.StringPtr("foo"), In: utils.StringPtr("foo")})
					pathItem.Parameters = append(pathItem.Parameters, Parameter{Name: utils.StringPtr("foo"), In: utils.StringPtr("baz")})
					pathItem.Parameters = append(pathItem.Parameters, Parameter{Name: utils.StringPtr("baz"), In: utils.StringPtr("baz")})
				})
				It("has validation messages", func() {
					validator = pathItem.Validate(validator)
					Expect(validator.HasMessages()).To(BeTrue())
					Expect(validator.GetMessages()).To(HaveLen(2))
				})
			})
		})
	})
})
