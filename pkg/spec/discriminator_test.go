package spec

import (
	. "gitlab.com/gdenis91/gopenapi/domain/spec"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/gdenis91/gopenapi/pkg/utils"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
)

var _ = Describe("Discriminator", func() {
	var (
		discriminator *Discriminator
		validator     *validation.ObjectValidator
	)

	BeforeEach(func() {
		validator = validation.NewObjectValidator("discriminator")
	})

	Context("with required fields", func() {
		BeforeEach(func() {
			discriminator = &Discriminator{
				PropertyName: utils.StringPtr("foo"),
			}
		})

		It("has no validation messages", func() {
			validator = discriminator.Validate(validator)
			Expect(validator.HasMessages()).To(BeFalse())
		})

		Context("propertyName attribute is nil", func() {
			BeforeEach(func() {
				discriminator.PropertyName = nil
			})
			It("has validation messages", func() {
				validator = discriminator.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})
	})
})
