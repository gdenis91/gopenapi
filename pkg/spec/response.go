package spec

import (
	"fmt"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
	"gopkg.in/yaml.v2"
	"github.com/pkg/errors"
)

// Response https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md#response-object
type Response struct {
	Ref         *string              `yaml:"$ref"`
	Description *string              `yaml:"description"`
	Headers     map[string]Header    `yaml:"headers"`
	Content     map[string]MediaType `yaml:"content"`
	Links       map[string]Link      `yaml:"links"`
}

// Validate validates the element
func (r *Response) Validate(validator *validation.ObjectValidator) *validation.ObjectValidator {
	validator.RequireNonEmptyString("description", r.Description)

	for k := range r.Links {
		validator.AppendMessageIf(!componentKeyRegExp.MatchString(k), fmt.Sprintf("'links' attribute key '%s' must match regular expression '%s'", componentKeyRegExp.String()))
	}

	return validator
}

func ResponseFromMap(responseMap map[interface{}]interface{}) (*Response, error) {
	docBytes, err := yaml.Marshal(responseMap)
	if err != nil {
		return nil, errors.Wrap(err, "could not marshal doc map to yaml bytes")
	}
	response := &Response{}
	err = yaml.Unmarshal(docBytes, &response)
	if err != nil {
		return nil, errors.Wrap(err, "could not unmarshal yaml bytes to schema")
	}
	return response, nil
}