package spec

import (
	. "gitlab.com/gdenis91/gopenapi/domain/spec"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
)

var _ = Describe("Header", func() {
	var (
		header    *Header
		validator *validation.ObjectValidator
	)

	BeforeEach(func() {
		validator = validation.NewObjectValidator("header")
	})

	Context("with required fields", func() {
		BeforeEach(func() {
			header = &Header{
				Schema: &Schema{},
			}
		})

		It("has no validation messages", func() {
			validator = header.Validate(validator)
			Expect(validator.HasMessages()).To(BeFalse())
		})

		Context("schema and content are nil", func() {
			BeforeEach(func() {
				header.Schema = nil
				header.Content = nil
			})
			It("has validation messages", func() {
				validator = header.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})

		Context("schema and content are set", func() {
			BeforeEach(func() {
				header.Content = map[string]MediaType{}
			})
			It("has validation messages", func() {
				validator = header.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})

		Context("only content is set with more than one entry", func() {
			BeforeEach(func() {
				header.Content = map[string]MediaType{
					"foo": {},
					"bar": {},
				}
			})
			It("has validation messages", func() {
				validator = header.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})
	})
})
