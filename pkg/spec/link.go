package spec

import (
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
	"gopkg.in/yaml.v2"
	"github.com/pkg/errors"
)

// Link https: //github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md#link-object
type Link struct {
	Ref          *string           `yaml:"$ref"`
	OperationRef *string           `yaml:"operationRef"`
	OperationID  *string           `yaml:"operationId"`
	Parameters   map[string]string `yaml:"parameters"`
	RequestBody  *interface{}      `yaml:"requestBody"`
	Description  *string           `yaml:"description"`
	Server       *Server           `yaml:"server"`
}

// Validate validates the element
func (l *Link) Validate(validator *validation.ObjectValidator) *validation.ObjectValidator {
	if l.OperationID == nil && l.OperationRef == nil {
		validator.AppendMessage("Either 'operationId' OR 'operationRef' must be specified")
	}

	if l.OperationID != nil && l.OperationRef != nil {
		validator.AppendMessage("Only 'operationId' OR 'operationRef' should be specified, not both")
	}

	return validator
}

func LinkFromMap(linkMap map[interface{}]interface{}) (*Link, error) {
	docBytes, err := yaml.Marshal(linkMap)
	if err != nil {
		return nil, errors.Wrap(err, "could not marshal doc map to yaml bytes")
	}
	link := &Link{}
	err = yaml.Unmarshal(docBytes, &link)
	if err != nil {
		return nil, errors.Wrap(err, "could not unmarshal yaml bytes to schema")
	}
	return link, nil
}