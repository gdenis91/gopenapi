package spec

import (
	"fmt"
	"gitlab.com/gdenis91/gopenapi/pkg/utils"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
)

// Operation https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md#operation-object
type Operation struct {
	Tags         []string               `yaml:"tags"`
	Summary      *string                `yaml:"summary"`
	Description  *string                `yaml:"description"`
	ExternalDocs *ExternalDocumentation `yaml:"externalDocs"`
	OperationID  *string                `yaml:"operationID"`
	Parameters   []Parameter            `yaml:"parameters"`
	RequestBody  *RequestBody           `yaml:"requestBody"`
	Responses    *Responses             `yaml:"responses"`
	Callbacks    map[string]Callback    `yaml:"callbacks"`
	Deprecated   *bool                  `yaml:"deprecated"`
	Security     []SecurityRequirement  `yaml:"security"`
	Servers      []Server               `yaml:"servers"`
}

// Validate validates the element
func (o *Operation) Validate(validator *validation.ObjectValidator) *validation.ObjectValidator {
	validator.RequireAttribute("responses", o.Responses)

	usedParams := map[string]Parameter{}
	for _, param := range o.Parameters {
		if _, found := usedParams[param.UniqueID()]; found {
			validator.AppendMessage(
				fmt.Sprintf("Duplicate param '%s' in '%s'. Each param must have a unique combination of 'name' and 'in' attribute values",
					utils.StringWithDefaultValue(param.Name, ""),
					utils.StringWithDefaultValue(param.In, "")))
		}

		usedParams[param.UniqueID()] = param
	}

	return validator
}
