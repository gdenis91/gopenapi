package spec

import (
	. "gitlab.com/gdenis91/gopenapi/domain/spec"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/gdenis91/gopenapi/pkg/utils"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
)

var _ = Describe("OauthFlow", func() {
	var (
		oauthFlow *OAuthFlow
		validator *validation.ObjectValidator
	)

	BeforeEach(func() {
		validator = validation.NewObjectValidator("oauthFlow")
	})

	Context("with required fields", func() {
		BeforeEach(func() {
			oauthFlow = &OAuthFlow{
				AuthorizationURL: utils.StringPtr("http://www.foo.com/"),
				TokenURL:         utils.StringPtr("http://www.foo.com/"),
				Scopes:           map[string]string{"foo": "bar"},
			}
		})

		It("has no validation messages", func() {
			validator = oauthFlow.Validate(validator)
			Expect(validator.HasMessages()).To(BeFalse())
		})

		Context("authorizationUrl attribute is nil", func() {
			BeforeEach(func() {
				oauthFlow.AuthorizationURL = nil
			})
			It("has validation messages", func() {
				validator = oauthFlow.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})

		Context("authorizationUrl is not a url", func() {
			BeforeEach(func() {
				oauthFlow.AuthorizationURL = utils.StringPtr("fiz;#wwfoo.com%*&()/")
			})
			It("has validation messages", func() {
				validator = oauthFlow.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})

		Context("tokenUrl attribute is nil", func() {
			BeforeEach(func() {
				oauthFlow.TokenURL = nil
			})
			It("has validation messages", func() {
				validator = oauthFlow.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})

		Context("tokenUrl is not a url", func() {
			BeforeEach(func() {
				oauthFlow.TokenURL = utils.StringPtr("fiz;#wwfoo.com%*&()/")
			})
			It("has validation messages", func() {
				validator = oauthFlow.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})

		Context("scopes attribute is nil", func() {
			BeforeEach(func() {
				oauthFlow.Scopes = nil
			})
			It("has validation messages", func() {
				validator = oauthFlow.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})

		Context("scopes attribute has no entries", func() {
			BeforeEach(func() {
				oauthFlow.Scopes = map[string]string{}
			})
			It("has validation messages", func() {
				validator = oauthFlow.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})

		Context("with refreshUrl attribute", func() {
			Context("which is a url", func() {
				BeforeEach(func() {
					oauthFlow.RefreshURL = utils.StringPtr("http://www.foo.com/")
				})
				It("has no validation messages", func() {
					validator = oauthFlow.Validate(validator)
					Expect(validator.HasMessages()).To(BeFalse())
				})
			})

			Context("which is not a url", func() {
				BeforeEach(func() {
					oauthFlow.RefreshURL = utils.StringPtr("fiz;#wwfoo.com%*&()/")
				})
				It("has validation messages", func() {
					validator = oauthFlow.Validate(validator)
					Expect(validator.HasMessages()).To(BeTrue())
				})
			})
		})

	})
})
