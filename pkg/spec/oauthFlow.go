package spec

import (
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
	"net/url"
)

// OAuthFlow https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md#oauth-flow-object
type OAuthFlow struct {
	AuthorizationURL *string           `yaml:"authorizationUrl"`
	TokenURL         *string           `yaml:"tokenUrl"`
	RefreshURL       *string           `yaml:"refreshUrl"`
	Scopes           map[string]string `yaml:"scopes"`
}

// Validate validates the element
func (o *OAuthFlow) Validate(validator *validation.ObjectValidator) *validation.ObjectValidator {
	validator.RequireNonEmptyString("authorizationUrl", o.AuthorizationURL)
	if o.AuthorizationURL != nil {
		_, err := url.Parse(*o.AuthorizationURL)
		validator.AppendMessageIfError(err, "'authorizationUrl' attribute must be in the format of a url")
	}

	validator.RequireNonEmptyString("tokenUrl", o.TokenURL)
	if o.TokenURL != nil {
		_, err := url.Parse(*o.TokenURL)
		validator.AppendMessageIfError(err, "'tokenUrl' attribute must be in the format of a url")
	}

	if o.RefreshURL != nil {
		_, err := url.Parse(*o.RefreshURL)
		validator.AppendMessageIfError(err, "'refreshUrl' attribute must be in the format of a url")
	}

	validator.RequireAttribute("scopes", o.Scopes)
	if o.Scopes != nil {
		validator.AppendMessageIf(len(o.Scopes) == 0, "Must specify at least one entry for 'scopes' attribute")
	}

	return validator
}
