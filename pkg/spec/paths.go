package spec

import (
	"fmt"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
	"strings"
)

// Paths https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md#paths-object
type Paths map[string]PathItem

// Validate validates the element
func (p *Paths) Validate(validator *validation.ObjectValidator) *validation.ObjectValidator {
	for k := range *p {
		validator.AppendMessageIf(!strings.HasPrefix(k, "/"), fmt.Sprintf("Element with key '%s' must start with '/'", k))
	}
	return validator
}
