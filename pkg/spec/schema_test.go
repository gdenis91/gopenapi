package spec

import (
	. "gitlab.com/gdenis91/gopenapi/domain/spec"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
	"gitlab.com/gdenis91/gopenapi/pkg/utils"
)

var _ = Describe("Schema", func() {
	var (
		schema *Schema
		validator *validation.ObjectValidator
	)

	BeforeEach(func() {
		validator = validation.NewObjectValidator("parameter")
	})

	Context("with required fields", func() {
		BeforeEach(func() {
			schema = &Schema{
				Type: utils.StringPtr("string"),
			}
		})

		It("has no validation messages", func() {
			validator = schema.Validate(validator)
			Expect(validator.HasMessages()).To(BeFalse())
		})

		Context("type attribute is unknown value", func() {
			BeforeEach(func() {
				schema.Type = utils.StringPtr("foo")
			})
			It("has validation messages", func() {
				validator = schema.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})

		Context("type attribute is array", func() {
			BeforeEach(func() {
				schema.Type = utils.StringPtr("array")
			})
			Context("items attribute not set", func() {
				BeforeEach(func() {
					schema.Items = nil
				})
				It("has validation messages", func() {
					validator = schema.Validate(validator)
					Expect(validator.HasMessages()).To(BeTrue())
				})
			})
			Context("items attribute set", func() {
				BeforeEach(func() {
					schema.Items = &Schema{}
				})
				It("has no validation messages", func() {
					validator = schema.Validate(validator)
					Expect(validator.HasMessages()).To(BeFalse())
				})
			})
		})

		Context("multipleOf attribute is less than 1", func() {
			BeforeEach(func() {
				schema.MultipleOf = utils.IntPtr(0)
			})
			It("has validation messages", func() {
				validator = schema.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})

		Context("maxLength attribute is less than 0", func() {
			BeforeEach(func() {
				schema.MaxLength = utils.IntPtr(-1)
			})
			It("has validation messages", func() {
				validator = schema.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})

		Context("minLength attribute is less than 0", func() {
			BeforeEach(func() {
				schema.MinLength = utils.IntPtr(-1)
			})
			It("has validation messages", func() {
				validator = schema.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})

		Context("maxItems attribute is less than 0", func() {
			BeforeEach(func() {
				schema.MaxItems = utils.IntPtr(-1)
			})
			It("has validation messages", func() {
				validator = schema.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})

		Context("minItems attribute is less than 0", func() {
			BeforeEach(func() {
				schema.MinItems = utils.IntPtr(-1)
			})
			It("has validation messages", func() {
				validator = schema.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})

		Context("maxProperties attribute is less than 0", func() {
			BeforeEach(func() {
				schema.MaxProperties = utils.IntPtr(-1)
			})
			It("has validation messages", func() {
				validator = schema.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})

		Context("minProperties attribute is less than 0", func() {
			BeforeEach(func() {
				schema.MinProperties = utils.IntPtr(-1)
			})
			It("has validation messages", func() {
				validator = schema.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})

		Context("required attribute has non-unique entries", func() {
			BeforeEach(func() {
				schema.Required = []string{"foo", "foo", "bar"}
			})
			It("has validation messages", func() {
				validator = schema.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})

		Context("pattern attribute is not valid regex", func() {
			BeforeEach(func() {
				schema.Pattern = utils.StringPtr("$$$$%^*&(&(Q)&!@()")
			})
			It("has validation messages", func() {
				validator = schema.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})

		Context("readOnly and writeOnly attributes are both true", func() {
			BeforeEach(func() {
				schema.ReadOnly = utils.BoolPtr(true)
				schema.WriteOnly = utils.BoolPtr(true)
			})
			It("has validation messages", func() {
				validator = schema.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})
	})
})
