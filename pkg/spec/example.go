package spec

import (
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
	"gopkg.in/yaml.v2"
	"github.com/pkg/errors"
)

// Example https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md#example-object
type Example struct {
	Ref           *string      `yaml:"$ref"`
	Summary       *string      `yaml:"summary"`
	Description   *string      `yaml:"description"`
	Value         *interface{} `yaml:"value"`
	ExternalValue *string      `yaml:"externalValue"`
}

// Validate validates the element
func (e *Example) Validate(validator *validation.ObjectValidator) *validation.ObjectValidator {
	return validator
}

func ExampleFromMap(exampleMap map[interface{}]interface{}) (*Example, error) {
	docBytes, err := yaml.Marshal(exampleMap)
	if err != nil {
		return nil, errors.Wrap(err, "could not marshal doc map to yaml bytes")
	}
	example := &Example{}
	err = yaml.Unmarshal(docBytes, &example)
	if err != nil {
		return nil, errors.Wrap(err, "could not unmarshal yaml bytes to schema")
	}
	return example, nil
}