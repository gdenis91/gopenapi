package spec

import (
	"fmt"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
	"gitlab.com/gdenis91/gopenapi/pkg/utils"
	"gopkg.in/yaml.v2"
	"github.com/pkg/errors"
)

// Parameter https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md#parameter-object
type Parameter struct {
	Ref             *string              `yaml:"$ref"`
	Name            *string              `yaml:"name"`
	In              *string              `yaml:"in"`
	Description     *string              `yaml:"description"`
	Required        *bool                `yaml:"required"`
	Deprecated      *bool                `yaml:"deprecated"`
	AllowEmptyValue *bool                `yaml:"allowEmptyValue"`
	Style           *string              `yaml:"style"`
	Explode         *bool                `yaml:"explode"`
	AllowReserved   *bool                `yaml:"allowReserved"`
	Schema          *Schema              `yaml:"schema"`
	Example         *interface{}         `yaml:"example"`
	Examples        map[string]Example   `yaml:"examples"`
	Content         map[string]MediaType `yaml:"content"`
}

// UniqueID returns the unique identifier for the parameter. The identifies is the concatination of the 'name' and 'in' attributes
func (p *Parameter) UniqueID() string {
	return utils.StringWithDefaultValue(p.Name, "") + ":" + utils.StringWithDefaultValue(p.In, "")
}

// Validate validates the element
func (p *Parameter) Validate(validator *validation.ObjectValidator) *validation.ObjectValidator {
	validator.RequireNonEmptyString("name", p.Name)
	validator.RequireNonEmptyString("in", p.In)

	parameterLocation := ParameterLocation(utils.StringWithDefaultValue(p.In, ""))
	if _, found := AllParameterLocations[parameterLocation]; !found {
		validator.AppendMessage(fmt.Sprintf("'in' attribute was %s, must have one of the following values: 'query', 'header', 'path', or 'cookie'", parameterLocation))
	}

	if parameterLocation == ParameterLocationPath {
		validator.RequireAttribute("required", p.Required)
		if p.Required != nil {
			validator.AppendMessageIf(!*p.Required, "'required' must be true if the parameter is in the path")
		}
	}

	if parameterLocation != ParameterLocationQuery {
		if p.AllowEmptyValue != nil {
			validator.AppendMessage("'allowEmptyValue' should only be specified for query parameters")
		}
	}

	if p.Schema == nil && p.Content == nil {
		validator.AppendMessage("Either 'schema' OR 'content' must be specified")
	}

	if p.Schema != nil && p.Content != nil {
		validator.AppendMessage("Only 'schema' OR 'content' should be specified, not both")
	}

	if p.Content != nil && len(p.Content) != 1 {
		validator.AppendMessage("If 'content' is specified it must contain exactly one entry")
	}

	return validator
}

func ParameterFromMap(parameterMap map[interface{}]interface{}) (*Parameter, error) {
	docBytes, err := yaml.Marshal(parameterMap)
	if err != nil {
		return nil, errors.Wrap(err, "could not marshal doc map to yaml bytes")
	}
	parameter := &Parameter{}
	err = yaml.Unmarshal(docBytes, &parameter)
	if err != nil {
		return nil, errors.Wrap(err, "could not unmarshal yaml bytes to schema")
	}
	return parameter, nil
}