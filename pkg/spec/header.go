package spec

import (
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
	"gopkg.in/yaml.v2"
	"github.com/pkg/errors"
)

// Header https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md#header-object
type Header struct {
	Ref             *string              `yaml:"$ref"`
	Description     *string              `yaml:"description"`
	Required        *bool                `yaml:"required"`
	Deprecated      *bool                `yaml:"deprecated"`
	AllowEmptyValue *bool                `yaml:"allowEmptyValue"`
	Style           *string              `yaml:"style"`
	Explode         *bool                `yaml:"explode"`
	AllowReserved   *bool                `yaml:"allowReserved"`
	Schema          *Schema              `yaml:"schema"`
	Example         *interface{}         `yaml:"example"`
	Examples        map[string]Example   `yaml:"examples"`
	Content         map[string]MediaType `yaml:"content"`
}

// Validate validates the element
func (h *Header) Validate(validator *validation.ObjectValidator) *validation.ObjectValidator {
	if h.Schema == nil && h.Content == nil {
		validator.AppendMessage("Either 'schema' OR 'content' must be specified")
	}

	if h.Schema != nil && h.Content != nil {
		validator.AppendMessage("Only 'schema' OR 'content' should be specified, not both")
	}

	if h.Content != nil && len(h.Content) != 1 {
		validator.AppendMessage("If 'content' is specified it must contain exactly one entry")
	}

	return validator
}

func HeaderFromMap(headerMap map[interface{}]interface{}) (*Header, error) {
	docBytes, err := yaml.Marshal(headerMap)
	if err != nil {
		return nil, errors.Wrap(err, "could not marshal doc map to yaml bytes")
	}
	header := &Header{}
	err = yaml.Unmarshal(docBytes, &header)
	if err != nil {
		return nil, errors.Wrap(err, "could not unmarshal yaml bytes to schema")
	}
	return header, nil
}