package spec

import (
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
	"gopkg.in/yaml.v2"
	"github.com/pkg/errors"
)

// Callback https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md#callback-object
type Callback map[string]PathItem

// Validate validates the element
func (c *Callback) Validate(validator *validation.ObjectValidator) *validation.ObjectValidator {
	return validator
}

func CallbackFromMap(callbackMap map[interface{}]interface{}) (*Callback, error) {
	docBytes, err := yaml.Marshal(callbackMap)
	if err != nil {
		return nil, errors.Wrap(err, "could not marshal doc map to yaml bytes")
	}
	callback := &Callback{}
	err = yaml.Unmarshal(docBytes, &callback)
	if err != nil {
		return nil, errors.Wrap(err, "could not unmarshal yaml bytes to schema")
	}
	return callback, nil
}