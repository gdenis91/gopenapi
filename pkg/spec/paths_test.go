package spec

import (
	. "gitlab.com/gdenis91/gopenapi/domain/spec"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
)

var _ = Describe("Paths", func() {
	var (
		paths     *Paths
		validator *validation.ObjectValidator
	)

	BeforeEach(func() {
		validator = validation.NewObjectValidator("info")
	})

	Context("with required fields", func() {
		BeforeEach(func() {
			paths = &Paths{
				"/foo": PathItem{},
			}
		})

		It("has no validation messages", func() {
			validator = paths.Validate(validator)
			Expect(validator.HasMessages()).To(BeFalse())
		})
	})

	Context("with item not starting with /", func() {
		BeforeEach(func() {
			paths = &Paths{
				"foo": PathItem{},
			}
		})

		It("has no validation messages", func() {
			validator = paths.Validate(validator)
			Expect(validator.HasMessages()).To(BeTrue())
		})
	})
})
