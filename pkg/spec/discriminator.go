package spec

import "gitlab.com/gdenis91/gopenapi/pkg/validation"

// Discriminator https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md#discriminator-object
type Discriminator struct {
	PropertyName *string           `yaml:"propertyName"`
	Mapping      map[string]string `yaml:"mapping"`
}

func (d *Discriminator) Validate(validator *validation.ObjectValidator) *validation.ObjectValidator {
	validator.RequireNonEmptyString("propertyName", d.PropertyName)
	return validator
}
