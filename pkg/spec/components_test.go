package spec

import (
	. "gitlab.com/gdenis91/gopenapi/domain/spec"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
)

var _ = Describe("Components", func() {
	const (
		validComponentKey   = "valid_key"
		invalidComponentKey = "invali$d_key"
	)

	var (
		components *Components
		validator  *validation.ObjectValidator
	)

	BeforeEach(func() {
		validator = validation.NewObjectValidator("components")
	})

	Context("with valid component keys", func() {
		BeforeEach(func() {
			components = &Components{
				Schemas:         map[string]Schema{validComponentKey: {}},
				Responses:       map[string]Response{validComponentKey: {}},
				Parameters:      map[string]Parameter{validComponentKey: {}},
				Examples:        map[string]Example{validComponentKey: {}},
				RequestBodies:   map[string]RequestBody{validComponentKey: {}},
				Headers:         map[string]Header{validComponentKey: {}},
				SecuritySchemes: map[string]SecurityScheme{validComponentKey: {}},
				Links:           map[string]Link{validComponentKey: {}},
				Callbacks:       map[string]Callback{validComponentKey: {}},
			}
		})

		It("has no validation messages", func() {
			validator = components.Validate(validator)
			Expect(validator.HasMessages()).To(BeFalse())
		})
	})

	Context("with invalid component keys", func() {
		BeforeEach(func() {
			components = &Components{
				Schemas:         map[string]Schema{invalidComponentKey: {}},
				Responses:       map[string]Response{invalidComponentKey: {}},
				Parameters:      map[string]Parameter{invalidComponentKey: {}},
				Examples:        map[string]Example{invalidComponentKey: {}},
				RequestBodies:   map[string]RequestBody{invalidComponentKey: {}},
				Headers:         map[string]Header{invalidComponentKey: {}},
				SecuritySchemes: map[string]SecurityScheme{invalidComponentKey: {}},
				Links:           map[string]Link{invalidComponentKey: {}},
				Callbacks:       map[string]Callback{invalidComponentKey: {}},
			}
		})

		It("has validation message", func() {
			validator = components.Validate(validator)
			Expect(validator.HasMessages()).To(BeTrue())
			Expect(validator.GetMessages()).To(HaveLen(9))
		})
	})
})
