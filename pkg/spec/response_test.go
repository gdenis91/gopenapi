package spec

import (
	. "gitlab.com/gdenis91/gopenapi/domain/spec"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/gdenis91/gopenapi/pkg/utils"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
)

var _ = Describe("Response", func() {
	const (
		validComponentKey   = "valid_key"
		invalidComponentKey = "invali$d_key"
	)

	var (
		response  *Response
		validator *validation.ObjectValidator
	)

	BeforeEach(func() {
		validator = validation.NewObjectValidator("response")
	})

	Context("with required fields", func() {
		BeforeEach(func() {
			response = &Response{
				Description: utils.StringPtr("foo"),
			}
		})

		It("has no validation messages", func() {
			validator = response.Validate(validator)
			Expect(validator.HasMessages()).To(BeFalse())
		})

		Context("description attribute is nil", func() {
			BeforeEach(func() {
				response.Description = nil
			})
			It("has validation messages", func() {
				validator = response.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})

		Context("with valid links keys", func() {
			BeforeEach(func() {
				response.Links = map[string]Link{validComponentKey: {}}
			})

			It("has no validation messages", func() {
				validator = response.Validate(validator)
				Expect(validator.HasMessages()).To(BeFalse())
			})
		})

		Context("with invalid links keys", func() {
			BeforeEach(func() {
				response.Links = map[string]Link{invalidComponentKey: {}}
			})

			It("has validation message", func() {
				validator = response.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})
	})
})
