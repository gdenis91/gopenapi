package spec

import (
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
	"fmt"
)

type Validator struct {
	followRefs bool
	remoteURL string
	refFollower RefFollower
}

func (v *Validator) Validate(doc *OpenAPIDoc) ([]string, error) {
	docMap, err := doc.AsMap()
	if err != nil {
		return nil, err
	}

	refFollower := RefFollower{
		rootDoc: docMap,
	}
	v.refFollower = refFollower

	baseValidator := validation.NewObjectValidator("openAPI")
	doc.Validate(baseValidator)
	v.validateInfo(baseValidator.AppendNestedValidationWithPrefix("info"), doc.Info)
	v.validateServers(baseValidator, doc.Servers)
	v.validatePaths(baseValidator, doc.Paths)
	v.validatePathComponents(baseValidator.AppendNestedValidationWithPrefix("components"), doc.Components)
	v.validateSecurityRequirements(baseValidator, doc.Security)
	v.validateTags(baseValidator, doc.Tags)
	v.validateExternalDocumentation(baseValidator.AppendNestedValidationWithPrefix("externalDocs"), doc.ExternalDocs)
	return baseValidator.GetMessages(), nil
}

func (v *Validator) validateInfo(validator *validation.ObjectValidator, info *Info) {
	if info == nil {
		return
	}
	info.Validate(validator)
	v.validateLicense(validator.AppendNestedValidationWithPrefix("license"), info.License)
	v.validateContact(validator.AppendNestedValidationWithPrefix("contact"), info.Contact)
}

func (v *Validator) validateContact(validator *validation.ObjectValidator, contact *Contact) {
	if contact == nil {
		return
	}
	contact.Validate(validator)
}

func (v *Validator) validateLicense(validator *validation.ObjectValidator, license *License) {
	if license == nil {
		return
	}
	license.Validate(validator)
}

func (v *Validator) validateServers(validator *validation.ObjectValidator, servers []Server) {
	for i, server := range servers {
		v.validateServer(validator.AppendNestedValidationWithPrefix(fmt.Sprintf("servers[%d]", i)), &server)
	}
}

func (v *Validator) validateServer(validator *validation.ObjectValidator, server *Server) {
	if server == nil {
		return
	}
	server.Validate(validator)
	v.validateServerVariables(validator, server.Variables)
}

func (v *Validator) validateServerVariables(validator *validation.ObjectValidator, serverVariables map[string]ServerVariable) {
	for k, serverVariable := range serverVariables {
		v.validateServerVariable(validator.AppendNestedValidationWithPrefix(fmt.Sprintf("variables[%s]", k)), &serverVariable)
	}
}

func (v *Validator) validateServerVariable(validator *validation.ObjectValidator, serverVariable *ServerVariable) {
	if serverVariable == nil {
		return
	}
	serverVariable.Validate(validator)
}

func (v *Validator) validatePathComponents(validator *validation.ObjectValidator, components *Components) {
	if components == nil {
		return
	}
	components.Validate(validator)
	v.validateSchemas(validator, components.Schemas)
	v.validateResponses(validator, components.Responses)
	v.validateParametersMap(validator, components.Parameters)
	v.validateExamples(validator, components.Examples)
	v.validateRequestBodies(validator, components.RequestBodies)
	v.validateHeaders(validator, components.Headers)
	v.validateSecuritySchemes(validator, components.SecuritySchemes)
	v.validateLinks(validator, components.Links)
	v.validateCallbacks(validator, components.Callbacks)
}

func (v *Validator) validatePaths(validator *validation.ObjectValidator, paths *Paths) {
	if paths == nil {
		return
	}
	paths.Validate(validator.AppendNestedValidationWithPrefix("paths"))
	for k, pathItem := range *paths {
		v.validatePathItem(validator.AppendNestedValidationWithPrefix(fmt.Sprintf("paths[%s]", k)), &pathItem)
	}
}

func (v *Validator) validatePathItem(validator *validation.ObjectValidator, pathItem *PathItem) {
	if pathItem == nil {
		return
	}
	pathItem.Validate(validator)
	v.validateOperation(validator.AppendNestedValidationWithPrefix("get"), pathItem.Get)
	v.validateOperation(validator.AppendNestedValidationWithPrefix("put"), pathItem.Put)
	v.validateOperation(validator.AppendNestedValidationWithPrefix("post"), pathItem.Post)
	v.validateOperation(validator.AppendNestedValidationWithPrefix("delete"), pathItem.Delete)
	v.validateOperation(validator.AppendNestedValidationWithPrefix("options"), pathItem.Options)
	v.validateOperation(validator.AppendNestedValidationWithPrefix("head"), pathItem.Head)
	v.validateOperation(validator.AppendNestedValidationWithPrefix("patch"), pathItem.Patch)
	v.validateOperation(validator.AppendNestedValidationWithPrefix("trace"), pathItem.Trace)
	v.validateServers(validator, pathItem.Servers)
	v.validateParameters(validator, pathItem.Parameters)
}

func (v *Validator) validateOperation(validator *validation.ObjectValidator, operation *Operation) {
	if operation == nil {
		return
	}
	operation.Validate(validator)
	v.validateExternalDocumentation(validator.AppendNestedValidationWithPrefix("externalDocs"), operation.ExternalDocs)
	v.validateParameters(validator, operation.Parameters)
	v.validateRequestBody(validator.AppendNestedValidationWithPrefix("requestBody"), operation.RequestBody)
	v.validateResponses(validator, operation.Responses)
	v.validateCallbacks(validator, operation.Callbacks)
	v.validateSecurityRequirements(validator, operation.Security)
	v.validateServers(validator, operation.Servers)
}

func (v *Validator) validateExternalDocumentation(validator *validation.ObjectValidator, externalDocumentation *ExternalDocumentation) {
	if externalDocumentation == nil {
		return
	}
	externalDocumentation.Validate(validator)
}

func (v *Validator) validateParametersMap(validator *validation.ObjectValidator, parameters map[string]Parameter) {
	for k, parameter := range parameters {
		v.validateParameter(validator.AppendNestedValidationWithPrefix(fmt.Sprintf("parameters[%s]", k)), &parameter)
	}
}

func (v *Validator) validateParameters(validator *validation.ObjectValidator, parameters []Parameter) {
	for i, parameter := range parameters {
		v.validateParameter(validator.AppendNestedValidationWithPrefix(fmt.Sprintf("parameters[%d]", i)), &parameter)
	}
}

func (v *Validator) validateParameter(validator *validation.ObjectValidator, parameter *Parameter) {
	if parameter == nil {
		return
	}
	parameter.Validate(validator)
	v.validateSchema(validator.AppendNestedValidationWithPrefix("schema"), parameter.Schema)
	v.validateExamples(validator, parameter.Examples)
	v.validateContent(validator, parameter.Content)
}

func (v *Validator) validateRequestBodies(validator *validation.ObjectValidator, requestBodies map[string]RequestBody) {
	for k, requestBody := range requestBodies {
		v.validateRequestBody(validator.AppendNestedValidationWithPrefix(fmt.Sprintf("requestBodies[%s]", k)), &requestBody)
	}
}

func (v *Validator) validateRequestBody(validator *validation.ObjectValidator, requestBody *RequestBody) {
	if requestBody == nil {
		return
	}
	requestBody.Validate(validator)
	v.validateContent(validator, requestBody.Content)
}

func (v *Validator) validateContent(validator *validation.ObjectValidator, content map[string]MediaType) {
	for k, mediaType := range content {
		v.validateMediaType(validator.AppendNestedValidationWithPrefix(fmt.Sprintf("content[%s]", k)), &mediaType)
	}
}

func (v *Validator) validateMediaType(validator *validation.ObjectValidator, mediaType *MediaType) {
	if mediaType == nil {
		return
	}
	mediaType.Validate(validator)
	v.validateSchema(validator.AppendNestedValidationWithPrefix("schema"), mediaType.Schema)
	v.validateExamples(validator, mediaType.Examples)
	v.validateEncodings(validator, mediaType.Encoding)
}

func (v *Validator) validateEncodings(validator *validation.ObjectValidator, encodings map[string]Encoding) {
	for k, encoding := range encodings {
		v.validateEncoding(validator.AppendNestedValidationWithPrefix(fmt.Sprintf("encodings[%s]", k)), &encoding)
	}
}

func (v *Validator) validateEncoding(validator *validation.ObjectValidator, encoding *Encoding) {
	if encoding == nil {
		return
	}
	encoding.Validate(validator)
	v.validateHeaders(validator, encoding.Headers)
}

func (v *Validator) validateResponses(validator *validation.ObjectValidator, responses *Responses) {
	if responses == nil {
		return
	}
	responses.Validate(validator.AppendNestedValidationWithPrefix("responses"))
	for k, response := range *responses {
		v.validateResponse(validator.AppendNestedValidationWithPrefix(fmt.Sprintf("responses[%s]", k)), &response)
	}
}

func (v *Validator) validateResponse(validator *validation.ObjectValidator, response *Response) {
	if response == nil {
		return
	}
	response.Validate(validator)
	v.validateHeaders(validator, response.Headers)
	v.validateContent(validator, response.Content)
	v.validateLinks(validator, response.Links)
}

func (v *Validator) validateCallbacks(validator *validation.ObjectValidator, callbacks map[string]Callback) {
	for k, callback := range callbacks {
		v.validateCallback(validator.AppendNestedValidationWithPrefix(fmt.Sprintf("callbacks[%s]", k)), &callback)
	}
}

func (v *Validator) validateCallback(validator *validation.ObjectValidator, callback *Callback) {
	if callback == nil {
		return
	}
	callback.Validate(validator)
}

func (v *Validator) validateExamples(validator *validation.ObjectValidator, examples map[string]Example) {
	for k, example := range examples {
		v.validateExample(validator.AppendNestedValidationWithPrefix(fmt.Sprintf("examples[%s]", k)), &example)
	}
}

func (v *Validator) validateExample(validator *validation.ObjectValidator, example *Example) {
	if example == nil {
		return
	}
	example.Validate(validator)
}

func (v *Validator) validateLinks(validator *validation.ObjectValidator, links map[string]Link) {
	for k, link := range links {
		v.validateLink(validator.AppendNestedValidationWithPrefix(fmt.Sprintf("links[%s]", k)), &link)
	}
}

func (v *Validator) validateLink(validator *validation.ObjectValidator, link *Link) {
	if link == nil {
		return
	}
	link.Validate(validator)
	v.validateServer(validator.AppendNestedValidationWithPrefix("server"), link.Server)
}

func (v *Validator) validateHeaders(validator *validation.ObjectValidator, headers map[string]Header) {
	for k, header := range headers {
		v.validateHeader(validator.AppendNestedValidationWithPrefix(fmt.Sprintf("headers[%s]", k)), &header)
	}
}

func (v *Validator) validateHeader(validator *validation.ObjectValidator, header *Header) {
	if header == nil {
		return
	}
	header.Validate(validator)
	v.validateSchema(validator.AppendNestedValidationWithPrefix("schema"), header.Schema)
	v.validateExamples(validator, header.Examples)
	v.validateContent(validator, header.Content)
}

func (v *Validator) validateTags(validator *validation.ObjectValidator, tags []Tag) {
	for i, tag := range tags {
		v.validateTag(validator.AppendNestedValidationWithPrefix(fmt.Sprintf("tags[%d]", i)), &tag)
	}
}

func (v *Validator) validateTag(validator *validation.ObjectValidator, tag *Tag) {
	if tag == nil {
		return
	}
	tag.Validate(validator)
	v.validateExternalDocumentation(validator.AppendNestedValidationWithPrefix("externalDocs"), tag.ExternalDocs)
}

func (v *Validator) validateProperties(validator *validation.ObjectValidator, schemas map[string]Schema) {
	for k, schema := range schemas {
		v.validateSchema(validator.AppendNestedValidationWithPrefix(fmt.Sprintf("properties[%s]", k)), &schema)
	}
}

func (v *Validator) validateAllOf(validator *validation.ObjectValidator, schemas []Schema) {
	for i, schema := range schemas {
		v.validateSchema(validator.AppendNestedValidationWithPrefix(fmt.Sprintf("allOf[%d]", i)), &schema)
	}
}

func (v *Validator) validateOneOf(validator *validation.ObjectValidator, schemas []Schema) {
	for i, schema := range schemas {
		v.validateSchema(validator.AppendNestedValidationWithPrefix(fmt.Sprintf("oneOf[%d]", i)), &schema)
	}
}

func (v *Validator) validateAnyOf(validator *validation.ObjectValidator, schemas []Schema) {
	for i, schema := range schemas {
		v.validateSchema(validator.AppendNestedValidationWithPrefix(fmt.Sprintf("anyOf[%d]", i)), &schema)
	}
}

func (v *Validator) validateItems(validator *validation.ObjectValidator, schemas []Schema) {
	for i, schema := range schemas {
		v.validateSchema(validator.AppendNestedValidationWithPrefix(fmt.Sprintf("items[%d]", i)), &schema)
	}
}

func (v *Validator) validateSchemas(validator *validation.ObjectValidator, schemas map[string]Schema) {
	for k, schema := range schemas {
		v.validateSchema(validator.AppendNestedValidationWithPrefix(fmt.Sprintf("schemas[%s]", k)), &schema)
	}
}

func (v *Validator) validateSchema(validator *validation.ObjectValidator, schema *Schema) {
	if schema == nil {
		return
	}

	if schema.Ref != nil && len(*schema.Ref) > 0 {
		err := v.refFollower.FollowRef(*schema.Ref, schema)
		if err != nil {
			validator.AppendMessage(fmt.Sprintf("could not follow $ref because '%v'", err))
			return
		}
	}

	schema.Validate(validator)
	v.validateAllOf(validator, schema.AllOf)
	v.validateOneOf(validator, schema.OneOf)
	v.validateAnyOf(validator, schema.AnyOf)
	v.validateSchema(validator.AppendNestedValidationWithPrefix("not"), schema.Not)
	v.validateSchema(validator.AppendNestedValidationWithPrefix("items"), schema.Items)
	v.validateProperties(validator, schema.Properties)
	v.validateSchema(validator.AppendNestedValidationWithPrefix("additionalProperties"), schema.AdditionalProperties)
	v.validateDiscriminator(validator.AppendNestedValidationWithPrefix("discriminator"), schema.Discriminator)
	v.validateExternalDocumentation(validator.AppendNestedValidationWithPrefix("externalDocs"), schema.ExternalDocs)
}

func (v *Validator) validateDiscriminator(validator *validation.ObjectValidator, discriminator *Discriminator) {
	if discriminator == nil {
		return
	}
	discriminator.Validate(validator)
}

func (v *Validator) validateSecuritySchemes(validator *validation.ObjectValidator, securitySchemes map[string]SecurityScheme) {
	for k, securityScheme := range securitySchemes {
		v.validateSecurityScheme(validator.AppendNestedValidationWithPrefix(fmt.Sprintf("securitySchemes[%s]", k)), &securityScheme)
	}
}

func (v *Validator) validateSecurityScheme(validator *validation.ObjectValidator, securityScheme *SecurityScheme) {
	if securityScheme == nil {
		return
	}
	securityScheme.Validate(validator)
	v.validateOAuthFlows(validator.AppendNestedValidationWithPrefix("flows"), securityScheme.Flows)
}

func (v *Validator) validateOAuthFlows(validator *validation.ObjectValidator, oauthFlows *OAuthFlows) {
	if oauthFlows == nil {
		return
	}
	oauthFlows.Validate(validator)
	v.validateOAuthFlow(validator.AppendNestedValidationWithPrefix("implicit"), oauthFlows.Implicit)
	v.validateOAuthFlow(validator.AppendNestedValidationWithPrefix("password"), oauthFlows.Password)
	v.validateOAuthFlow(validator.AppendNestedValidationWithPrefix("clientCredentials"), oauthFlows.ClientCredentials)
	v.validateOAuthFlow(validator.AppendNestedValidationWithPrefix("authorizationCode"), oauthFlows.AuthorizationCode)
}

func (v *Validator) validateOAuthFlow(validator *validation.ObjectValidator, oauthFlow *OAuthFlow) {
	if oauthFlow == nil {
		return
	}
	oauthFlow.Validate(validator)
}

func (v *Validator) validateSecurityRequirements(validator *validation.ObjectValidator, securityRequirements []SecurityRequirement) {
	for i, securityRequirement := range securityRequirements {
		v.validateSecurityRequirement(validator.AppendNestedValidationWithPrefix(fmt.Sprintf("security[%d]", i)), &securityRequirement)
	}
}

func (v *Validator) validateSecurityRequirement(validator *validation.ObjectValidator, securityRequirement *SecurityRequirement) {
	if securityRequirement == nil {
		return
	}
	securityRequirement.Validate(validator)
}
