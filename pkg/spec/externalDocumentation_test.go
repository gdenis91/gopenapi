package spec

import (
	. "gitlab.com/gdenis91/gopenapi/domain/spec"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/gdenis91/gopenapi/pkg/utils"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
)

var _ = Describe("ExternalDocumentation", func() {
	var (
		externalDocumentation *ExternalDocumentation
		validator             *validation.ObjectValidator
	)

	BeforeEach(func() {
		validator = validation.NewObjectValidator("externalDocumentation")
	})

	Context("with required fields", func() {
		BeforeEach(func() {
			externalDocumentation = &ExternalDocumentation{
				URL: utils.StringPtr("www.foo.com"),
			}
		})

		It("has no validation messages", func() {
			validator = externalDocumentation.Validate(validator)
			Expect(validator.HasMessages()).To(BeFalse())
		})

		Context("with url attribute", func() {
			Context("which is a url", func() {
				BeforeEach(func() {
					externalDocumentation.URL = utils.StringPtr("http://www.foo.com/")
				})
				It("has no validation messages", func() {
					validator = externalDocumentation.Validate(validator)
					Expect(validator.HasMessages()).To(BeFalse())
				})
			})

			Context("which is not a url", func() {
				BeforeEach(func() {
					externalDocumentation.URL = utils.StringPtr("fiz;#wwfoo.com%*&()/")
				})
				It("has validation messages", func() {
					validator = externalDocumentation.Validate(validator)
					Expect(validator.HasMessages()).To(BeTrue())
				})
			})
		})

		Context("url attribute is nil", func() {
			BeforeEach(func() {
				externalDocumentation.URL = nil
			})
			It("has validation messages", func() {
				validator = externalDocumentation.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})
	})
})
