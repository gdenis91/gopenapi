package spec

import (
	. "gitlab.com/gdenis91/gopenapi/domain/spec"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/gdenis91/gopenapi/pkg/utils"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
)

var _ = Describe("SecurityScheme", func() {
	var (
		securityScheme *SecurityScheme
		validator      *validation.ObjectValidator
	)

	BeforeEach(func() {
		validator = validation.NewObjectValidator("securityScheme")
	})

	Context("apiKey type with required fields", func() {
		BeforeEach(func() {
			securityScheme = &SecurityScheme{
				Type: utils.StringPtr("apiKey"),
				Name: utils.StringPtr("foo"),
				In:   utils.StringPtr("query"),
			}
		})

		It("has no validation messages", func() {
			validator = securityScheme.Validate(validator)
			Expect(validator.HasMessages()).To(BeFalse())
		})

		Context("name attribute is nil", func() {
			BeforeEach(func() {
				securityScheme.Name = nil
			})
			It("has validation messages", func() {
				validator = securityScheme.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})

		Context("in attribute is nil", func() {
			BeforeEach(func() {
				securityScheme.In = nil
			})
			It("has validation messages", func() {
				validator = securityScheme.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})

		Context("in attribute is unknown value", func() {
			BeforeEach(func() {
				securityScheme.In = utils.StringPtr("foo")
			})
			It("has validation messages", func() {
				validator = securityScheme.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})
	})

	Context("http type with required fields", func() {
		BeforeEach(func() {
			securityScheme = &SecurityScheme{
				Type:   utils.StringPtr("http"),
				Scheme: utils.StringPtr("foo"),
			}
		})

		It("has no validation messages", func() {
			validator = securityScheme.Validate(validator)
			Expect(validator.HasMessages()).To(BeFalse())
		})

		Context("scheme attribute is nil", func() {
			BeforeEach(func() {
				securityScheme.Scheme = nil
			})
			It("has validation messages", func() {
				validator = securityScheme.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})
	})

	Context("oauth2 type with required fields", func() {
		BeforeEach(func() {
			securityScheme = &SecurityScheme{
				Type:  utils.StringPtr("oauth2"),
				Flows: &OAuthFlows{},
			}
		})

		It("has no validation messages", func() {
			validator = securityScheme.Validate(validator)
			Expect(validator.HasMessages()).To(BeFalse())
		})

		Context("flows attribute is nil", func() {
			BeforeEach(func() {
				securityScheme.Flows = nil
			})
			It("has validation messages", func() {
				validator = securityScheme.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})
	})

	Context("openIdConnect type with required fields", func() {
		BeforeEach(func() {
			securityScheme = &SecurityScheme{
				Type:             utils.StringPtr("openIdConnect"),
				OpenIDConnectURL: utils.StringPtr("http://www.google.com"),
			}
		})

		It("has no validation messages", func() {
			validator = securityScheme.Validate(validator)
			Expect(validator.HasMessages()).To(BeFalse())
		})

		Context("openIDConnectURL attribute is nil", func() {
			BeforeEach(func() {
				securityScheme.OpenIDConnectURL = nil
			})
			It("has validation messages", func() {
				validator = securityScheme.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})

		Context("openIDConnectURL attribute is not a url", func() {
			BeforeEach(func() {
				securityScheme.OpenIDConnectURL = utils.StringPtr("fiz;#wwfoo.com%*&()/")
			})
			It("has validation messages", func() {
				validator = securityScheme.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})
	})

	Context("unknown type", func() {
		BeforeEach(func() {
			securityScheme = &SecurityScheme{
				Type: utils.StringPtr("foo"),
			}
		})

		It("has validation messages", func() {
			validator = securityScheme.Validate(validator)
			Expect(validator.HasMessages()).To(BeTrue())
		})
	})
})
