package spec

import (
	. "gitlab.com/gdenis91/gopenapi/domain/spec"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/gdenis91/gopenapi/pkg/utils"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
)

var _ = Describe("Parameter", func() {
	var (
		parameter *Parameter
		validator *validation.ObjectValidator
	)

	BeforeEach(func() {
		validator = validation.NewObjectValidator("parameter")
	})

	Context("with required fields", func() {
		BeforeEach(func() {
			parameter = &Parameter{
				Name:   utils.StringPtr("foo"),
				In:     utils.StringPtr("query"),
				Schema: &Schema{},
			}
		})

		It("has no validation messages", func() {
			validator = parameter.Validate(validator)
			Expect(validator.HasMessages()).To(BeFalse())
		})

		Context("name attribute is nil", func() {
			BeforeEach(func() {
				parameter.Name = nil
			})
			It("has validation messages", func() {
				validator = parameter.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})

		Context("in attribute is nil", func() {
			BeforeEach(func() {
				parameter.In = nil
			})
			It("has validation messages", func() {
				validator = parameter.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})

		Context("in attribute is unknown value", func() {
			BeforeEach(func() {
				parameter.In = utils.StringPtr("foo")
			})
			It("has validation messages", func() {
				validator = parameter.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})

		Context("in attribute is path", func() {
			BeforeEach(func() {
				parameter.In = utils.StringPtr("path")
			})
			It("has validation messages", func() {
				validator = parameter.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})

			Context("allowEmptyValue is set", func() {
				BeforeEach(func() {
					parameter.AllowEmptyValue = utils.BoolPtr(true)
				})
				It("has validation messages", func() {
					validator = parameter.Validate(validator)
					Expect(validator.HasMessages()).To(BeTrue())
				})
			})

			Context("required is true", func() {
				BeforeEach(func() {
					parameter.Required = utils.BoolPtr(true)
				})
				It("has validation messages", func() {
					validator = parameter.Validate(validator)
					Expect(validator.HasMessages()).To(BeFalse())
				})
			})

			Context("required is false", func() {
				BeforeEach(func() {
					parameter.Required = utils.BoolPtr(false)
				})
				It("has validation messages", func() {
					validator = parameter.Validate(validator)
					Expect(validator.HasMessages()).To(BeTrue())
				})
			})
		})

		Context("schema and content are nil", func() {
			BeforeEach(func() {
				parameter.Schema = nil
				parameter.Content = nil
			})
			It("has validation messages", func() {
				validator = parameter.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})

		Context("schema and content are set", func() {
			BeforeEach(func() {
				parameter.Content = map[string]MediaType{}
			})
			It("has validation messages", func() {
				validator = parameter.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})

		Context("only content is set with more than one entry", func() {
			BeforeEach(func() {
				parameter.Content = map[string]MediaType{
					"foo": {},
					"bar": {},
				}
			})
			It("has validation messages", func() {
				validator = parameter.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})
	})
})
