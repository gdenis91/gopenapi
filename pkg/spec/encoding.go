package spec

import "gitlab.com/gdenis91/gopenapi/pkg/validation"

// Encoding https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md#encoding-object
type Encoding struct {
	ContentType   *string           `yaml:"contentType"`
	Headers       map[string]Header `yaml:"headers"`
	Style         *string           `yaml:"style"`
	Explode       *bool             `yaml:"explode"`
	AllowReserved *bool             `yaml:"allowReserved"`
}

// Validate validates the element
func (e *Encoding) Validate(validator *validation.ObjectValidator) *validation.ObjectValidator {
	return validator
}
