package spec

import (
	. "gitlab.com/gdenis91/gopenapi/domain/spec"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
)

var _ = Describe("Responses", func() {
	var (
		responses *Responses
		validator *validation.ObjectValidator
	)

	BeforeEach(func() {
		validator = validation.NewObjectValidator("responses")
	})

	Context("with required fields", func() {
		BeforeEach(func() {
			responses = &Responses{
				"200": Response{},
			}
		})

		It("has no validation messages", func() {
			validator = responses.Validate(validator)
			Expect(validator.HasMessages()).To(BeFalse())
		})

		Context("only content is set with more than one entry", func() {
			BeforeEach(func() {
				responses = &Responses{}
			})
			It("has validation messages", func() {
				validator = responses.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})
	})
})
