package spec

import (
	"fmt"
	"gitlab.com/gdenis91/gopenapi/pkg/utils"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
	"net/url"
	"gopkg.in/yaml.v2"
	"github.com/pkg/errors"
)

// SecurityScheme https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md#security-scheme-object
type SecurityScheme struct {
	Ref              *string     `yaml:"$ref"`
	Type             *string     `yaml:"type"`
	Description      *string     `yaml:"description"`
	Name             *string     `yaml:"name"`
	In               *string     `yaml:"in"`
	Scheme           *string     `yaml:"scheme"`
	BearerFormat     *string     `yaml:"bearerFormat"`
	Flows            *OAuthFlows `yaml:"flows"`
	OpenIDConnectURL *string     `yaml:"openIdConnectUrl"`
}

// Validate validates the element
func (ss *SecurityScheme) Validate(validator *validation.ObjectValidator) *validation.ObjectValidator {
	validator.RequireNonEmptyString("type", ss.Type)

	securitySchemeType := SecuritySchemeType(utils.StringWithDefaultValue(ss.Type, ""))
	if _, found := AllSecuritySchemeTypes[securitySchemeType]; !found {
		validator.AppendMessage(fmt.Sprintf("'type' attribute was %s, must have one of the following values: 'apiKey', 'http', 'oauth2', or 'openIdConnect'", securitySchemeType))
	}

	if securitySchemeType == SecuritySchemeTypeAPIKey {
		validator.AppendMessageIf(ss.Name == nil, "'name' is required if 'type' is 'apiKey'")
		validator.AppendMessageIf(ss.In == nil, "'in' is required if 'type' is 'apiKey'")
		parameterLocation := ParameterLocation(utils.StringWithDefaultValue(ss.In, ""))
		if _, found := SecuritySchemeParameterLocations[parameterLocation]; !found {
			validator.AppendMessage(fmt.Sprintf("'in' attribute was %s, must have one of the following values: 'query', 'header', or 'cookie'", parameterLocation))
		}
	}

	if securitySchemeType == SecuritySchemeTypeHTTP {
		validator.AppendMessageIf(ss.Scheme == nil, "'scheme' is required if 'type' is 'http'")
	}

	if securitySchemeType == SecuritySchemeTypeOAuth2 {
		validator.AppendMessageIf(ss.Flows == nil, "'flows' is required if 'type' is 'oauth2'")
	}

	if securitySchemeType == SecuritySchemeTypeOpenIDConnect {
		validator.AppendMessageIf(ss.OpenIDConnectURL == nil, "'openIdConnectUrl' is required if 'type' is 'openIdConnect'")
		if ss.OpenIDConnectURL != nil {
			_, err := url.Parse(*ss.OpenIDConnectURL)
			validator.AppendMessageIfError(err, "'openIdConnectUrl' attribute must be in the format of a url")
		}
	}

	return validator
}

func SecuritySchemeFromMap(securitySchemeMap map[interface{}]interface{}) (*SecurityScheme, error) {
	docBytes, err := yaml.Marshal(securitySchemeMap)
	if err != nil {
		return nil, errors.Wrap(err, "could not marshal doc map to yaml bytes")
	}
	securityScheme := &SecurityScheme{}
	err = yaml.Unmarshal(docBytes, &securityScheme)
	if err != nil {
		return nil, errors.Wrap(err, "could not unmarshal yaml bytes to schema")
	}
	return securityScheme, nil
}