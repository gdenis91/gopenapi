package spec

import (
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
)

// OAuthFlows https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md#oauth-flows-object
type OAuthFlows struct {
	Implicit          *OAuthFlow `yaml:"implicit"`
	Password          *OAuthFlow `yaml:"password"`
	ClientCredentials *OAuthFlow `yaml:"clientCredentials"`
	AuthorizationCode *OAuthFlow `yaml:"authorizationCode"`
}

// Validate validates the element
func (o *OAuthFlows) Validate(validator *validation.ObjectValidator) *validation.ObjectValidator {
	return validator
}
