package spec

import (
	"io/ioutil"
	"github.com/pkg/errors"
	"fmt"
	"gopkg.in/yaml.v2"
)

func LoadSpec(filepath string) (*OpenAPIDoc, error) {
	specBytes, err := ioutil.ReadFile(filepath)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("could not read spec file %s", filepath))
	}

	doc := &OpenAPIDoc{}
	err = yaml.Unmarshal(specBytes, doc)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("could not unmarshal spec file %s", filepath))
	}
	return doc, nil
}