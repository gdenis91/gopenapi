package spec

import (
	. "gitlab.com/gdenis91/gopenapi/domain/spec"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/gdenis91/gopenapi/pkg/utils"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
)

var _ = Describe("License", func() {
	var (
		license   *License
		validator *validation.ObjectValidator
	)

	BeforeEach(func() {
		validator = validation.NewObjectValidator("license")
	})

	Context("with required fields", func() {
		BeforeEach(func() {
			license = &License{
				Name: utils.StringPtr("foo"),
			}
		})

		It("has no validation messages", func() {
			validator = license.Validate(validator)
			Expect(validator.HasMessages()).To(BeFalse())
		})

		Context("with url attribute", func() {
			Context("which is a url", func() {
				BeforeEach(func() {
					license.URL = utils.StringPtr("http://www.foo.com/")
				})
				It("has no validation messages", func() {
					validator = license.Validate(validator)
					Expect(validator.HasMessages()).To(BeFalse())
				})
			})

			Context("which is not a url", func() {
				BeforeEach(func() {
					license.URL = utils.StringPtr("fiz;#wwfoo.com%*&()/")
				})
				It("has validation messages", func() {
					validator = license.Validate(validator)
					Expect(validator.HasMessages()).To(BeTrue())
				})
			})
		})

		Context("name attribute is nil", func() {
			BeforeEach(func() {
				license.Name = nil
			})
			It("has validation messages", func() {
				validator = license.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})
	})
})
