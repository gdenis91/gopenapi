package spec

import (
	. "gitlab.com/gdenis91/gopenapi/domain/spec"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/gdenis91/gopenapi/pkg/utils"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
)

var _ = Describe("Link", func() {
	var (
		link      *Link
		validator *validation.ObjectValidator
	)

	BeforeEach(func() {
		validator = validation.NewObjectValidator("link")
	})

	Context("with required fields", func() {
		BeforeEach(func() {
			link = &Link{
				OperationID: utils.StringPtr("foo"),
			}
		})

		It("has no validation messages", func() {
			validator = link.Validate(validator)
			Expect(validator.HasMessages()).To(BeFalse())
		})

		Context("operationId and operationRef are nil", func() {
			BeforeEach(func() {
				link.OperationRef = nil
				link.OperationID = nil
			})
			It("has validation messages", func() {
				validator = link.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})

		Context("operationId and operationRef are set", func() {
			BeforeEach(func() {
				link.OperationRef = utils.StringPtr("bar")
			})
			It("has validation messages", func() {
				validator = link.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})
	})
})
