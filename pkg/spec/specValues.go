package spec

import "regexp"

var (
	componentKeyRegExp = regexp.MustCompile(`^[a-zA-Z0-9.\-_]+$`)
)

type ParameterLocation string

func (pl ParameterLocation) String() string {
	return string(pl)
}

const (
	ParameterLocationPath   ParameterLocation = "path"
	ParameterLocationQuery  ParameterLocation = "query"
	ParameterLocationHeader ParameterLocation = "header"
	ParameterLocationCookie ParameterLocation = "cookie"
)

var (
	AllParameterLocations = map[ParameterLocation]ParameterLocation{
		ParameterLocationPath:   ParameterLocationPath,
		ParameterLocationQuery:  ParameterLocationQuery,
		ParameterLocationHeader: ParameterLocationHeader,
		ParameterLocationCookie: ParameterLocationCookie,
	}

	SecuritySchemeParameterLocations = map[ParameterLocation]ParameterLocation{
		ParameterLocationQuery:  ParameterLocationQuery,
		ParameterLocationHeader: ParameterLocationHeader,
		ParameterLocationCookie: ParameterLocationCookie,
	}
)

type SecuritySchemeType string

func (sst SecuritySchemeType) String() string {
	return string(sst)
}

const (
	SecuritySchemeTypeAPIKey        SecuritySchemeType = "apiKey"
	SecuritySchemeTypeHTTP          SecuritySchemeType = "http"
	SecuritySchemeTypeOAuth2        SecuritySchemeType = "oauth2"
	SecuritySchemeTypeOpenIDConnect SecuritySchemeType = "openIdConnect"
)

var (
	AllSecuritySchemeTypes = map[SecuritySchemeType]SecuritySchemeType{
		SecuritySchemeTypeAPIKey:        SecuritySchemeTypeAPIKey,
		SecuritySchemeTypeHTTP:          SecuritySchemeTypeHTTP,
		SecuritySchemeTypeOAuth2:        SecuritySchemeTypeOAuth2,
		SecuritySchemeTypeOpenIDConnect: SecuritySchemeTypeOpenIDConnect,
	}
)

type SchemaType string

func (s SchemaType) String() string {
	return string(s)
}

const (
	SchemaTypeString  SchemaType = "string"
	SchemaTypeNumber  SchemaType = "number"
	SchemaTypeInteger SchemaType = "integer"
	SchemaTypeBoolean SchemaType = "boolean"
	SchemaTypeArray   SchemaType = "array"
	SchemaTypeObject  SchemaType = "object"
	SchemaTypeEmpty   SchemaType = ""
)

var (
	AllSchemaTypes = map[SchemaType]SchemaType{
		SchemaTypeString:  SchemaTypeString,
		SchemaTypeNumber:  SchemaTypeNumber,
		SchemaTypeInteger: SchemaTypeInteger,
		SchemaTypeBoolean: SchemaTypeBoolean,
		SchemaTypeArray:   SchemaTypeArray,
		SchemaTypeObject:  SchemaTypeObject,
		SchemaTypeEmpty:   SchemaTypeEmpty,
	}
)

type SchemaTypeFormat string

func (s SchemaTypeFormat) String() string {
	return string(s)
}

const (
	SchemaTypeFormatFloat    SchemaTypeFormat = "float"
	SchemaTypeFormatDouble   SchemaTypeFormat = "double"
	SchemaTypeFormatInt32    SchemaTypeFormat = "int32"
	SchemaTypeFormatInt64    SchemaTypeFormat = "int64"
	SchemaTypeFormatDate     SchemaTypeFormat = "date"
	SchemaTypeFormatDateTime SchemaTypeFormat = "date-time"
	SchemaTypeFormatByte     SchemaTypeFormat = "byte"
	SchemaTypeFormatBinary   SchemaTypeFormat = "binary"
)

var (
	KnownSchemaTypeFormats = map[SchemaTypeFormat]SchemaTypeFormat{
		SchemaTypeFormatFloat:    SchemaTypeFormatFloat,
		SchemaTypeFormatDouble:   SchemaTypeFormatDouble,
		SchemaTypeFormatInt32:    SchemaTypeFormatInt32,
		SchemaTypeFormatInt64:    SchemaTypeFormatInt64,
		SchemaTypeFormatDate:     SchemaTypeFormatDate,
		SchemaTypeFormatDateTime: SchemaTypeFormatDateTime,
		SchemaTypeFormatByte:     SchemaTypeFormatByte,
		SchemaTypeFormatBinary:   SchemaTypeFormatBinary,
	}

	StringSchemaTypeFormats = map[SchemaTypeFormat]SchemaTypeFormat{
		SchemaTypeFormatDate:     SchemaTypeFormatDate,
		SchemaTypeFormatDateTime: SchemaTypeFormatDateTime,
		SchemaTypeFormatByte:     SchemaTypeFormatByte,
		SchemaTypeFormatBinary:   SchemaTypeFormatBinary,
	}

	NumberSchemaTypeFormats = map[SchemaTypeFormat]SchemaTypeFormat{
		SchemaTypeFormatFloat:  SchemaTypeFormatFloat,
		SchemaTypeFormatDouble: SchemaTypeFormatDouble,
		SchemaTypeFormatInt32:  SchemaTypeFormatInt32,
		SchemaTypeFormatInt64:  SchemaTypeFormatInt64,
	}
)
