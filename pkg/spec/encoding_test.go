package spec

import (
	. "gitlab.com/gdenis91/gopenapi/domain/spec"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
)

var _ = Describe("Encoding", func() {
	var (
		encoding  *Encoding
		validator *validation.ObjectValidator
	)

	BeforeEach(func() {
		validator = validation.NewObjectValidator("encoding")
	})

	Context("with required fields", func() {
		BeforeEach(func() {
			encoding = &Encoding{}
		})

		It("has no validation messages", func() {
			validator = encoding.Validate(validator)
			Expect(validator.HasMessages()).To(BeFalse())
		})
	})
})
