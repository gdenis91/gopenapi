package spec

import (
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
	"gopkg.in/yaml.v2"
	"github.com/pkg/errors"
)

// RequestBody https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md#request-body-object
type RequestBody struct {
	Ref         *string              `yaml:"$ref"`
	Description *string              `yaml:"description"`
	Content     map[string]MediaType `yaml:"content"`
	Required    *bool                `yaml:"required"`
}

// Validate validates the element
func (rb *RequestBody) Validate(validator *validation.ObjectValidator) *validation.ObjectValidator {
	validator.RequireAttribute("content", rb.Content)

	if rb.Content != nil && len(rb.Content) == 0 {
		validator.AppendMessage("must specify at least one entry for 'content'")
	}

	return validator
}

func RequestBodyFromMap(requestBodyMap map[interface{}]interface{}) (*RequestBody, error) {
	docBytes, err := yaml.Marshal(requestBodyMap)
	if err != nil {
		return nil, errors.Wrap(err, "could not marshal doc map to yaml bytes")
	}
	requestBody := &RequestBody{}
	err = yaml.Unmarshal(docBytes, &requestBody)
	if err != nil {
		return nil, errors.Wrap(err, "could not unmarshal yaml bytes to schema")
	}
	return requestBody, nil
}