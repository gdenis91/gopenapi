package spec

import (
	"gitlab.com/gdenis91/gopenapi/pkg/utils"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
)

// Tag https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md#tag-object
type Tag struct {
	Name         *string                `yaml:"name"`
	Description  *string                `yaml:"description"`
	ExternalDocs *ExternalDocumentation `yaml:"externalDocs"`
}

// UniqueID returns the unique identifier for the parameter. The identifies is the concatination of the 'name' and 'in' attributes
func (t *Tag) UniqueID() string {
	return utils.StringWithDefaultValue(t.Name, "")
}

// Validate validates the element
func (t *Tag) Validate(validator *validation.ObjectValidator) *validation.ObjectValidator {
	validator.RequireNonEmptyString("name", t.Name)

	return validator
}
