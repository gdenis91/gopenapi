package spec

import (
	. "gitlab.com/gdenis91/gopenapi/domain/spec"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
)

var _ = Describe("OauthFLows", func() {
	var (
		oauthFlows *OAuthFlows
		validator  *validation.ObjectValidator
	)

	BeforeEach(func() {
		validator = validation.NewObjectValidator("oauthFlows")
	})

	Context("with required fields", func() {
		BeforeEach(func() {
			oauthFlows = &OAuthFlows{}
		})

		It("has no validation messages", func() {
			validator = oauthFlows.Validate(validator)
			Expect(validator.HasMessages()).To(BeFalse())
		})
	})
})
