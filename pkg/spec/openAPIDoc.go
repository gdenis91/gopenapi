package spec

import (
	"fmt"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
	"gopkg.in/yaml.v2"
	"github.com/pkg/errors"
)

// OpenAPIDoc https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md#openapi-object
type OpenAPIDoc struct {
	OpenAPI      *string                `yaml:"openapi"`
	Info         *Info                  `yaml:"info"`
	Servers      []Server               `yaml:"servers"`
	Paths        *Paths                 `yaml:"paths"`
	Components   *Components            `yaml:"components"`
	Security     []SecurityRequirement  `yaml:"security"`
	Tags         []Tag                  `yaml:"tags"`
	ExternalDocs *ExternalDocumentation `yaml:"externalDocs"`
}

// Validate validates the object against the specification
func (o *OpenAPIDoc) Validate(validator *validation.ObjectValidator) *validation.ObjectValidator {
	validator.RequireExactString("openapi", "3.0.0", o.OpenAPI)
	validator.RequireAttribute("info", o.Info)
	validator.RequireAttribute("paths", o.Paths)

	tagsByName := map[string]Tag{}
	for _, tag := range o.Tags {
		if _, found := tagsByName[tag.UniqueID()]; found {
			validator.AppendMessage(fmt.Sprintf("Duplicate tag name '%s' found in 'tags' attribute. Each tag name must be unique"))
		}

		tagsByName[tag.UniqueID()] = tag
	}

	return validator
}

func (o *OpenAPIDoc) AsMap() (map[interface{}]interface{}, error) {
	docBytes, err := yaml.Marshal(*o)
	if err != nil {
		return nil, errors.Wrap(err, "could not marshal doc to yaml bytes")
	}
	var docData interface{}
	err = yaml.Unmarshal(docBytes, &docData)
	if err != nil {
		return nil, errors.Wrap(err, "could not unmarshal yaml bytes to interface")
	}
	docMap, isMap := docData.(map[interface{}]interface{})
	if !isMap{
		return nil, errors.New("docData was not a map")
	}
	return docMap, nil
}

func OpenAPIDocFromMap(docMap map[string]interface{}) (*OpenAPIDoc, error) {
	docBytes, err := yaml.Marshal(docMap)
	if err != nil {
		return nil, errors.Wrap(err, "could not marshal doc map to yaml bytes")
	}
	openAPIDoc := &OpenAPIDoc{}
	err = yaml.Unmarshal(docBytes, &openAPIDoc)
	if err != nil {
		return nil, errors.Wrap(err, "could not unmarshal yaml bytes to openAPIDoc")
	}
	return openAPIDoc, nil
}
