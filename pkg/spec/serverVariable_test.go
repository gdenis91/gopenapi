package spec

import (
	. "gitlab.com/gdenis91/gopenapi/domain/spec"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/gdenis91/gopenapi/pkg/utils"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
)

var _ = Describe("ServerVariable", func() {
	var (
		serverVariable *ServerVariable
		validator      *validation.ObjectValidator
	)

	BeforeEach(func() {
		validator = validation.NewObjectValidator("serverVariable")
	})

	Context("with required fields", func() {
		BeforeEach(func() {
			serverVariable = &ServerVariable{
				Default: utils.StringPtr("testVal"),
			}
		})

		It("has no validation messages", func() {
			validator = serverVariable.Validate(validator)
			Expect(validator.HasMessages()).To(BeFalse())
		})

		Context("default attribute is nil", func() {
			BeforeEach(func() {
				serverVariable.Default = nil
			})
			It("has validation messages", func() {
				validator = serverVariable.Validate(validator)
				Expect(validator.HasMessages()).To(BeTrue())
			})
		})
	})
})
