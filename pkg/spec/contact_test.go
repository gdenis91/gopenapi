package spec

import (
	. "gitlab.com/gdenis91/gopenapi/domain/spec"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/gdenis91/gopenapi/pkg/utils"
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
)

var _ = Describe("Contact", func() {
	var (
		contact   *Contact
		validator *validation.ObjectValidator
	)

	BeforeEach(func() {
		validator = validation.NewObjectValidator("contact")
	})

	Context("with required fields", func() {
		BeforeEach(func() {
			contact = &Contact{}
		})

		It("has no validation messages", func() {
			validator = contact.Validate(validator)
			Expect(validator.HasMessages()).To(BeFalse())
		})

		Context("with termsOfService attribute", func() {
			Context("which is a url", func() {
				BeforeEach(func() {
					contact.URL = utils.StringPtr("http://www.foo.com/")
				})
				It("has no validation messages", func() {
					validator = contact.Validate(validator)
					Expect(validator.HasMessages()).To(BeFalse())
				})
			})

			Context("which is not a url", func() {
				BeforeEach(func() {
					contact.URL = utils.StringPtr("fiz;#wwfoo.com%*&()/")
				})
				It("has validation messages", func() {
					validator = contact.Validate(validator)
					Expect(validator.HasMessages()).To(BeTrue())
				})
			})
		})

		Context("with email attribute", func() {
			Context("which is an email address", func() {
				BeforeEach(func() {
					contact.Email = utils.StringPtr("foo@bar.com")
				})
				It("has no validation messages", func() {
					validator = contact.Validate(validator)
					Expect(validator.HasMessages()).To(BeFalse())
				})
			})

			Context("which is not an email addres", func() {
				BeforeEach(func() {
					contact.Email = utils.StringPtr("fiz-baz*com")
				})
				It("has validation messages", func() {
					validator = contact.Validate(validator)
					Expect(validator.HasMessages()).To(BeTrue())
				})
			})
		})
	})
})
