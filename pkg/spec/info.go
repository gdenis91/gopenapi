package spec

import (
	"gitlab.com/gdenis91/gopenapi/pkg/validation"
	"net/url"
)

// Info https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md#info-object
type Info struct {
	Title          *string  `yaml:"title"`
	Description    *string  `yaml:"description"`
	TermsOfService *string  `yaml:"termsOfService"`
	Contact        *Contact `yaml:"contact"`
	License        *License `yaml:"license"`
	Version        *string  `yaml:"version"`
}

// Validate validates the element
func (i *Info) Validate(validator *validation.ObjectValidator) *validation.ObjectValidator {
	validator.RequireNonEmptyString("title", i.Title)
	validator.RequireNonEmptyString("version", i.Version)

	if i.TermsOfService != nil {
		_, err := url.Parse(*i.TermsOfService)
		validator.AppendMessageIfError(err, "'termsOfService' attribute must be in the format of a url")
	}

	return validator
}
