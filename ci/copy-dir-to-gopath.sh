#!/usr/bin/env bash

echo "Linking /builds to /go/src/gitlab.com"
ln -s /builds /go/src/gitlab.com
