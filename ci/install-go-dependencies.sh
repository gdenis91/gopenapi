#!/usr/bin/env bash

# Set up glide
curl https://glide.sh/get | sh
# Install golint
go get -u github.com/golang/lint/golint
# Install ginkgo
go get github.com/onsi/ginkgo/ginkgo