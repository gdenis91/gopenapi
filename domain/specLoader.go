package domain

import (
	"github.com/pkg/errors"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

func LoadSpec(filepath string) (OpenAPIDoc, error) {
	apiBytes, err := ioutil.ReadFile(filepath)
	if err != nil {
		return OpenAPIDoc{}, errors.Wrap(err, "could not read file")
	}

	doc := OpenAPIDoc{}
	err = yaml.Unmarshal(apiBytes, &doc)
	if err != nil {
		return doc, errors.Wrap(err, "could not unmarshal file")
	}
	return doc, nil
}
