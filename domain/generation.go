package domain

import (
	j "github.com/dave/jennifer/jen"
	"gitlab.com/gdenis91/gopenapi/cmd/gopenapi/utils"
	"strings"
	"strconv"
)

var (
	opPointer = "*"
	opEqual = "="
	refSeparator = "/"
)

func (s *Schema) GenerateType(modelName string) ([]j.Code, error) {
	var generatedModels []j.Code
	model := j.Type().Id(modelName)

	buildSchema(s, model)

	var additionalCode []j.Code
	if len(s.Enum) > 0 {
		additionalCode = append(additionalCode, getEnumDeclarations(s, modelName)...)
	}

	generatedModels = append(generatedModels, model)
	generatedModels = append(generatedModels, additionalCode...)
	return generatedModels, nil
}

func buildSchema(s *Schema, model *j.Statement) {
	if utils.BoolWithDefaultValue(s.Nullable, false) {
		model.Op(opPointer)
	}

	if s.Ref != nil {
		model.Id(getModelNameFromRef(*s.Ref))
		return
	}

	if len(s.AllOf) > 0 || len(s.AnyOf) > 0 || len(s.OneOf) > 0 {
		model.Interface()
		return
	}

	schemaType := utils.StringWithDefaultValue(s.Type, DataTypeObject.String())
	switch SchemaDataType(schemaType) {
	case DataTypeString:
		buildString(s, model)
	case DataTypeNumber:
		buildNumber(s, model)
	case DataTypeInteger:
		buildInteger(s, model)
	case DataTypeBoolean:
		buildBool(s, model)
	case DataTypeArray:
		buildArray(s, model)
	case DataTypeObject:
		buildObject(s, model)
	default:
		utils.Println("WARNING: Unknown schema type '%s'", schemaType)
	}
}

func buildString(s *Schema, model *j.Statement) {
	model.String()
}

func buildNumber(s *Schema, model *j.Statement) {
	model.Float32()
}

func buildInteger(s *Schema, model *j.Statement) {
	numberFormat := utils.StringWithDefaultValue(s.Format, "")
	switch NumberFormat(numberFormat) {
	case NumberFormatInt32:
		model.Int32()
	case NumberFormatInt64:
		model.Int64()
	default:
		model.Int()
	}
}

func buildBool(s *Schema, model *j.Statement) {
	model.Bool()
}

func buildArray(s *Schema, model *j.Statement) {
	buildSchema(s.Items, model.Index())
}

func buildObject(s *Schema, model *j.Statement) {
	if s.AdditionalProperties != nil {
		model.Map(j.String())
		buildSchema(s.AdditionalProperties, model)
		return
	}

	if len(s.Properties) == 0 {
		model.Map(j.String()).Interface()
		return
	}

	var properties []j.Code
	for propertyName, propertySchema := range s.Properties {
		propertyModel := j.Id(propertyName)
		buildSchema(propertySchema, propertyModel)
		properties = append(properties, propertyModel)
	}

	model.Struct(properties...)
}

func getEnumDeclarations(s *Schema, modelName string) []j.Code {
	var code []j.Code
	for _, val := range s.Enum {
		switch typeVal := val.(type) {
		case string:
			varName := modelName + uppercaseFirstLetter(typeVal)
			code = append(code, j.Var().Id(varName).Id(modelName).Id(opEqual).Lit(typeVal))
		case int:
			code = append(code, j.Var().Id(modelName + strconv.Itoa(typeVal)).Id(modelName).Id(opEqual).Lit(typeVal))
		default:
			utils.Println("WARNING: Unsupported enum value '%v'", typeVal)
		}
	}
	return code
}

func getModelNameFromRef(ref string) string {
	refParts := strings.Split(ref, refSeparator)
	return refParts[len(refParts) - 1]
}

func uppercaseFirstLetter(value string) string {
	vals := strings.Split(value, "")
	vals[0] = strings.ToUpper(vals[0])
	return strings.Join(vals, "")
}