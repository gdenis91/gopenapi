package domain

type SchemaDataType string

func (s SchemaDataType) String() string {
	return string(s)
}

const (
	DataTypeString  SchemaDataType = "string"
	DataTypeNumber  SchemaDataType = "number"
	DataTypeInteger SchemaDataType = "integer"
	DataTypeBoolean SchemaDataType = "boolean"
	DataTypeArray   SchemaDataType = "array"
	DataTypeObject  SchemaDataType = "object"
)

type NumberFormat string

func (n NumberFormat) String() string {
	return string(n)
}

const (
	NumberFormatFloat  NumberFormat = "float"
	NumberFormatDouble NumberFormat = "double"
	NumberFormatInt32  NumberFormat = "int32"
	NumberFormatInt64  NumberFormat = "int64"
)

type StringFormat string

func (s StringFormat) String() string {
	return string(s)
}

const (
	StringFormatDate     StringFormat = "date"
	StringFormatDateTime StringFormat = "date-time"
	StringFormatByte     StringFormat = "byte"
	StringFormatBinary   StringFormat = "binary"
	// ToDo: Add any custom string format types
)
