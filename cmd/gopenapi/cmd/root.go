package cmd

import (
	"fmt"
	"os"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var cfgFile string

const (
	cfgVerbose = "verbose"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "gopenapi",
	Short: "go command line utility for interacting with openapi (fka swagger) 3.0 documents",
	Long: `gopenapi is a command line utility for interacting with OpenAPI 3.0 documents (formerly known as Swagger documents).

This tool can perform a variety of functions including
spec validation, generation of client and server code,
generation of documentation, and basic api validation`,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.gopenapi.yaml)")

	rootCmd.PersistentFlags().BoolP(cfgVerbose, "v", false, "log more verbose messages in tool output")

	viper.BindPFlags(rootCmd.PersistentFlags())
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// Search config in home directory with name ".gopenapi" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".gopenapi")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}

func handleError(err error, message string, terminateIfErr bool) {
	if err != nil {
		fmt.Println(message)
		if viper.GetBool(cfgVerbose) {
			fmt.Println(fmt.Sprintf("%+v", err))
		}
		if terminateIfErr {
			os.Exit(1)
		}
	}
}

func clearOutput() {
	print("\033[H\033[2J")
}