package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/gdenis91/gopenapi/pkg/spec"
	"gitlab.com/gdenis91/gopenapi/pkg/utils"
	"os"
)

const (
	cfgValidateNoFollowRefs = "no-follow-refs"
	cfgValidateWatchChanges = "watch"
	cfgValidateSpecEndpoint = "endpoint"

	cfgValidateDefaultSpecFilename = "validate.defaultSpecFilename"
)

// validateCmd represents the validate command
var validateCmd = &cobra.Command{
	Use:   "validate [flags] [full path to spec]",
	Short: "validate an openapi spec",
	Long: `
A command for validating an openapi spec, validate takes
a root specification document as input and will validate
the full specification following $ref links by default.
Flags can be specified that disable following $ref's, as
well as a flag that watches a specification for changes
and reruns validation'.`,
	Run: func(cmd *cobra.Command, args []string) {
		// Get the root openapi spec
		specFilename := viper.GetString(cfgValidateDefaultSpecFilename)
		if len(args) > 0 {
			specFilename = args[0]
		}

		validateHandler := ValidateHandler{
			filename:     specFilename,
			watchChanges: viper.GetBool(cfgValidateWatchChanges),
		}

		validateHandler.HandleValidation()
	},
	Args: cobra.MaximumNArgs(1),
}

func init() {
	rootCmd.AddCommand(validateCmd)

	//validateCmd.PersistentFlags().Bool(cfgValidateNoFollowRefs, false, "do not follow $ref links in specification")
	validateCmd.PersistentFlags().BoolP(cfgValidateWatchChanges, "w", false, "watch specification for changes and re-run validation")
	//validateCmd.PersistentFlags().StringP(cfgValidateSpecEndpoint, "e", "", "full address of web endpoint serving the specification")

	viper.BindPFlags(validateCmd.PersistentFlags())

	viper.SetDefault(cfgValidateDefaultSpecFilename, "openapi.yaml")
}

type ValidateHandler struct {
	filename     string
	watchChanges bool
}

func (v *ValidateHandler) HandleValidation() {
	// If we are watching for changes register the save handler
	watchSpecChan := make(chan bool)
	if v.watchChanges {
		utils.RegisterFileSaveHandler(v.filename, func() {
			utils.PrintClear()
			v.validateSpec()
		}, func(err error) {
			handleError(err, fmt.Sprintf("error watching file %s", v.filename), true)
		})
	}

	// Validate the spec for the first time
	isValid := v.validateSpec()

	// If we are watching changes we block and allow the save handler to take over validation runs
	if v.watchChanges {
		<-watchSpecChan
	}

	// If we made it this far we aren't watching for changes so we can exit
	// with the status of the first validation run
	if !isValid {
		os.Exit(1)
	}
}

func (v *ValidateHandler) validateSpec() bool {
	openAPIDoc, err := spec.LoadSpec(v.filename)
	if err != nil {
		fmt.Println("Spec has errors:")
		handleError(err, fmt.Sprintf("could not load spec at location '%s'", v.filename), false)
		return false
	}

	// Then we validate the spec
	validator := spec.Validator{}
	messages, err := validator.Validate(openAPIDoc)
	if err != nil {
		fmt.Println("Spec has errors:")
		handleError(err, fmt.Sprintf("could not validate spec at location '%s'", v.filename), false)
		return false
	}

	if len(messages) == 0 {
		fmt.Println("Spec is valid!")
		return true
	}

	fmt.Println("Spec has errors:")
	for _, message := range messages {
		fmt.Println(message)
	}
	return false
}
