package domain

type PetType int

const (
	Dog PetType = iota
	Cat
	Bird
	Hamster
	Pig
)

type Gender string

const (
	Male    Gender = "male"
	Female  Gender = "female"
	Unknown Gender = "Unknown"
)

type Pet struct {
	ID        string
	Type      PetType
	Name      string
	Rating    int
	IsMammal  bool
	DetailsID string
}

type PetDetails struct {
	ID        string
	Gender    Gender
	Age       int
	SpecialID *string
	HasLegs   bool
}
