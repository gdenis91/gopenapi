/devices?offset=&limit=
{
    "meta": {
        "traceID": "",
        "poweredBy": "",
        "paging": {
            "start": 0,
            "limit": 0,
            "offset": 0,
            "total": 0
        }
    },
    "resources": [
        {

        }
    ]
}

/devices/{id}

/devices/{id}/users

/devices/{id}/users/{userID}

/groups

/groups/{id}

/groups/{id}/members

/groups/{id}/members/{id}

/users

/users/{id}

/users/{id}/devices

/users/{id}/devices/{id}



