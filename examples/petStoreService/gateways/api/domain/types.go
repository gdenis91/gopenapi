package domain

type PetStoreID string

type Species string

const (
	SpeciesDog     Species = "dog"
	SpeciesCat     Species = "cat"
	SpeciesBird    Species = "bird"
	SpeciesHamster Species = "hamster"
	SpeciesPig     Species = "pig"
)

type Gender string

const (
	GenderMale Gender = "male"
	GenderFemale Gender = "female"
)

type Pet struct {
	Name string `json:"name"`
	Tag  string `json:"tag"`
	ID   int    `json:"id"`
}

type NewPet struct {
	Name string `json:"name"`
	Tag  string `json:"tag"`
}

type Error struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}
