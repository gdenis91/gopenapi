package main

import (
	"github.com/kataras/iris"
)

func main() {
	app := iris.Default()

	registerIrisRoutes(app)
}

func registerIrisRoutes(app *iris.Application) {
	app.Get("/pets", findPets)
	app.Post("/pets", addPet)
	app.Get("/pets/{id}", findPetByID)
	app.Delete("/pets/{id}", deletePet)
}


