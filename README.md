Supported Features
---------
1) Validation of OpenAPI 3.0.0 Specifications
2) Support for navigating $ref links in specification
3) Generation of Component/Model Objects with validation
    - Generation of models for use as API requests and responses
    - Generation of models for database persistence (Could go even further and generate ES schemas/update scripts)
    - Models support validation method to check values against defined constraints
4) Generation of Server Scaffolding with validation
    - Initial support for gorestful
    - Custom filters to perform request validations
5) Generation of http Clients with validation
6) Generation of Documentation





ToDo Test Spec:
[X] Numbers
    [X] Number type
    [X] Integer type
    [X] Format support
    [ ] Validator for min/max (inclusive + exclusive)
    [ ] Validator for 'multipleOf'
[ ] Strings
    [X] String type
    [ ] Format support
    [ ] Validator for len (min + max)
    [ ] Validator for pattern
[X] Booleans
    [X] Bool type
[X] Null
    [X] Nullable types
[ ] Arrays
    [X] Array type
    [X] Primitive items
    [X] Object items
    [X] $ref for items
    [X] Nested Arrays
    [X] Mixed-type array
    [ ] Validator for len (min + max)
    [ ] Validator for uniqueness
[ ] Objects
    [X] Object type
    [X] Nested objects
    [X] $ref for nested objects
    [ ] Validator for number of properties for freeform objects
    [ ] Validator for required fields
    [ ] Validator for read/write only fields
[ ] Enums
    [X] Enum types
    [ ] Enum helper functions (Stringers, etc...)
[ ] Maps/Freeform objects
    [X] Map type
    [ ] Support 'AdditionalProperties' as boolean or empty object
    [X] $ref map object values
    [ ] Validator for fixed keys
[ ] Inheritance/Polymorphism
